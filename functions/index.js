/* eslint-disable promise/no-nesting */
/* eslint-disable promise/always-return */
const express = require('express');
const app = express();
app.use(express.json());
'use strict';
 
const functions = require('firebase-functions');
const admin = require('firebase-admin');
// const cors = require('cors')({ origin: true});
const assistant = require('actions-on-google');
const SessionsClient = require('dialogflow');
const serviceAccount = require('./moodtracker-20fb0-firebase-adminsdk-74mm0-22ffe0abe1.json')
//applicationDefault()
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://moodtracker-20fb0.firebaseio.com/'
});

const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');

 
exports.helloworld = functions.https.onRequest( (req , res ) => {
    res.send('hello');
});

exports.dialogflowGateway = functions.https.onRequest((request, response) => {
  (request, response, async () => {
    const { queryInput, sessionId } = request.body;


    const sessionClient = new SessionsClient({ credentials: serviceAccount  });
    const session = sessionClient.sessionPath('MoodTracker', sessionId);


    const responses = await sessionClient.detectIntent({ session, queryInput});

    const result = responses[0].queryResult;

    response.send(result);
  });
});


process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements



exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
  var uid =  "0UAgusrzrMg6Aq28qp3s5kbMr3l2" ;
  // firebase.auth().currentUser.getIdToken(true)
  // .then((token) => {
  //   admin.auth().verifyIdToken(token)
  //   .then((decodedToken) => {
  //      var uidUser = decodedToken.uid;
  //      uidUser = uid ;
  //   }).catch((error) => {
  //       console.error(error);
  //   });
  // }).catch((err) => {
  //   console.error(err);
  // });
  // functions.auth.user().options

  function scoreTotal() {  
      const completeDT = new Date() ;
      const getTime = completeDT.toLocaleString();
      return admin.database().ref(`depressScore/${uid}`).once('value').then( (snapshot) => {
        var total1 =  snapshot.child('total1').val();
        var total2 =  snapshot.child('total2').val();
        var total3 =  snapshot.child('total3').val();
        var total4 =  snapshot.child('total4').val();
        var total5 =  snapshot.child('total5').val();
        var total6 =  snapshot.child('total6').val();
        var total7 =  snapshot.child('total7').val();
        var total8 =  snapshot.child('total8').val();
        var total9 =  snapshot.child('total9').val();
        var sum = total1+total2+total3+total4+total5+total6+total7+total8+total9;
        
        if(sum<7){ 
          agent.add(`คะแนนของคุณคือ${sum} คุณไม่มีภาวะซึมเศร้า`)
          admin.database().ref(`depressScore/${uid}`).child('result').push({ result :'ไม่มีภาวะซึมเศร้า' , totalScore :sum , completeDT: getTime  }); 
        } else if(sum>=7 && sum<=12) { 
          agent.add(`คะแนนของคุณคือ${sum} คุณมีภาวะซึมเศร้าระดับน้อย`);
          admin.database().ref(`depressScore/${uid}`).child('result').push({ result :'ภาวะซึมเศร้าระดับน้อย' , totalScore :sum ,completeDT: getTime }); 
        } else if(sum>=13 && sum<=18) { 
          agent.add(`คะแนนของคุณคือ${sum} คุณมีภาวะซึมเศร้าระดับกลาง`);
          admin.database().ref(`depressScore/${uid}`).child('result').push({ result :'ภาวะซึมเศร้าระดับกลาง' , totalScore :sum ,completeDT: getTime }); 
        } else if(sum >19) {
          agent.add(`คะแนนของคุณคือ${sum} คุณมีภาวะซึมเศร้าระดับรุนแรง`);
          admin.database().ref(`depressScore/${uid}`).child('result').push({ result :'ภาวะซึมเศร้าระดับรุนแรง' , totalScore :sum , completeDT: getTime }); 
        }   
      
  
      });

   }
  
    function scoreSuicide() {
      return admin.database().ref('suicideScore').once('value').then( (snapshot) => {
        var total1 =  snapshot.child('total1').val();
        var total2 =  snapshot.child('total2').val();
        var total3 =  snapshot.child('total3').val();
        var total4 =  snapshot.child('total4').val();
        var total5 =  snapshot.child('total5').val();
        var total6 =  snapshot.child('total6').val();
        var total7 =  snapshot.child('total7').val();
        var total8 =  snapshot.child('total8').val();
        var total9 =  snapshot.child('total9').val();
        var sum = total1+total2+total3+total4+total5+total6+total7+total8+total9;
        
        if(sum===0){ 
          agent.add(`คะแนนของคุณคือ${sum} คุณไม่มีภาวะซึมเศร้า`);
        } else if(sum>=1 && sum<=8) { 
          agent.add(`คะแนนของคุณคือ${sum} แนวโน้มฆ่าตัวตายเล็กน้อย`);
        } else if(sum>=9 && sum<=16) { 
          agent.add(`คะแนนของคุณคือ${sum} แนวโน้มฆ่าตัวตายระดับปานกลาง`);
        } else if(sum >17) {
          agent.add(`คะแนนของคุณคือ${sum} แนวโน้มฆ่าตัวตายระดับรุนแรง`);
        }   
      } );
   }
  
    function scoreMentalHealth() {
      return admin.database().ref('happinessScore').once('value').then( (snapshot) => {
        var total1 =  snapshot.child('total1').val();
        var total2 =  snapshot.child('total2').val();
        var total3 =  snapshot.child('total3').val();
        var total4 =  snapshot.child('total4').val();
        var total5 =  snapshot.child('total5').val();
        var total6 =  snapshot.child('total6').val();
        var total7 =  snapshot.child('total7').val();
        var total8 =  snapshot.child('total8').val();
        var total9 =  snapshot.child('total9').val();
        var total10 =  snapshot.child('total10').val();
        var total11 =  snapshot.child('total11').val();
        var total12 =  snapshot.child('total12').val();
        var total13 =  snapshot.child('total13').val();
        var total14 =  snapshot.child('total14').val();
        var total15 =  snapshot.child('total15').val();
        var sum = total1+total2+total3+total4+total5+total6+total7+total8+total9+total10+total11+total12+total13 +
        total14 +total15;
      
        if(sum>=51 && sum<=60){ 
          agent.add(`คะแนนของคุณคือ${sum} สุขภาพจิตดีกว่าคนทั่วไป`);
        } else if(sum>=44 && sum<=50) { 
          agent.add(`คะแนนของคุณคือ${sum} สุขภาพจิตเท่ากับคนทั่วไป`);
        } else if(sum<=43 ) { 
          agent.add(`คะแนนของคุณคือ${sum} สุขภาพจิตต่ำกว่าคนทั่วไป`);
        }  
      } );
   }
  
   function scoreStress() {
      return admin.database().ref(`stressScore/${uid}`).once('value').then( (snapshot) => {
        var total1 =  snapshot.child('total1').val();
        var total2 =  snapshot.child('total2').val();
        var total3 =  snapshot.child('total3').val();
        var total4 =  snapshot.child('total4').val();
        var total5 =  snapshot.child('total5').val();
        var sum = total1+total2+total3+total4+total5;
        const completeDT = new Date() ;
        const getTime = completeDT.toLocaleString();
        
        if(sum>=0 && sum<=4){ 
          agent.add(`คะแนนของคุณคือ${sum} เครียดน้อย`);
          admin.database().ref(`stressScore/${uid}`).child('result').push({ result :'เครียดน้อย' , totalScore :sum ,completeDT: getTime }); 
        } else if(sum>=5 && sum<=7) { 
          agent.add(`คะแนนของคุณคือ${sum} เครียดปานกลาง`);
          admin.database().ref(`stressScore/${uid}`).child('result').push({ result :'เครียดปานกลาง' , totalScore :sum ,completeDT: getTime });      
        } else if(sum>=8 && sum<=9) { 
          agent.add(`คะแนนของคุณคือ${sum} เครียดมาก`);
          admin.database().ref(`stressScore/${uid}`).child('result').push({ result :'เครียดมาก' , totalScore :sum ,completeDT: getTime }); 
        } else if(sum>=10 && sum<=15) {
          agent.add(`คะแนนของคุณคือ${sum} เครียดมากที่สุด`);
          admin.database().ref(`stressScore/${uid}`).child('result').push({ result :'เครียดมากที่สุด' , totalScore :sum ,completeDT: getTime }); 
        }   
      } );
   }
  
  
  function depressCalculate1(agent) {
    let responde = request.body.queryResult.parameters.score ;
	  let score ;
   	if(responde === 'ไม่มีเลย') { score = 0 ; } 
    else if ( responde === 'เป็นบางวัน') { score = 1 ; }
    else if (responde === 'เป็นบ่อย'){score = 2 ; }
    else if( responde === 'เป็นทุกวัน'){ score = 3 ; } 
    return admin.database().ref(`depressScore/${uid}`).child('total1').set(score);    
}
    function depressCalculate2(agent) {
    let responde = request.body.queryResult.parameters.score ;
	  let score ;
   	if(responde === 'ไม่มีเลย') { score = 0; } 
    else if ( responde === 'เป็นบางวัน') { score = 1 ; }
    else if (responde === 'เป็นบ่อย'){score = 2 ; }
    else if( responde === 'เป็นทุกวัน'){ score = 3 ; }   
    return admin.database().ref(`depressScore/${uid}`).child('total2').set(score);    
    
}
   function depressCalculate3(agent) {
    let responde = request.body.queryResult.parameters.score ;
	  let score ;
   	if(responde === 'ไม่มีเลย') { score = 0 ; } 
    else if ( responde === 'เป็นบางวัน') { score = 1 ; }
    else if (responde === 'เป็นบ่อย'){score = 2 ; }
    else if( responde === 'เป็นทุกวัน'){ score = 3 ; }    
    return admin.database().ref(`depressScore/${uid}`).child('total3').set(score);    
}
   function depressCalculate4(agent) {
    let responde = request.body.queryResult.parameters.score ;
	let score ;
   	if(responde === 'ไม่มีเลย') { score = 0 ; } 
    else if ( responde === 'เป็นบางวัน') { score = 1 ; }
    else if (responde === 'เป็นบ่อย'){score = 2 ; }
    else if( responde === 'เป็นทุกวัน'){ score = 3 ; }    
    return admin.database().ref(`depressScore/${uid}`).child('total4').set(score);    
}
   function depressCalculate5(agent) {
    let responde = request.body.queryResult.parameters.score ;
	let score ;
   	if(responde === 'ไม่มีเลย') { score = 0 ; } 
    else if ( responde === 'เป็นบางวัน') { score = 1 ; }
    else if (responde === 'เป็นบ่อย'){score = 2 ; }
    else if( responde === 'เป็นทุกวัน'){ score =3 ; }    
    return admin.database().ref(`depressScore/${uid}`).child('total5').set(score);    
}
   function depressCalculate6(agent) {
    let responde = request.body.queryResult.parameters.score ;
	let score ;
   	if(responde === 'ไม่มีเลย') { score = 0 ; } 
    else if ( responde === 'เป็นบางวัน') { score = 1 ; }
    else if (responde === 'เป็นบ่อย'){score = 2 ; }
    else if( responde === 'เป็นทุกวัน'){ score = 3 ; }    
    return admin.database().ref(`depressScore/${uid}`).child('total6').set(score);    
}
   function depressCalculate7(agent) {
    let responde = request.body.queryResult.parameters.score ;
	let score ;
   	if(responde === 'ไม่มีเลย') { score = 0 ; } 
    else if ( responde === 'เป็นบางวัน') { score = 1 ; }
    else if (responde === 'เป็นบ่อย'){score = 2 ; }
    else if( responde === 'เป็นทุกวัน'){ score = 3 ; }    
    return admin.database().ref(`depressScore/${uid}`).child('total7').set(score);    
}
   function depressCalculate8(agent) {
    let responde = request.body.queryResult.parameters.score ;
	let score ;
   	if(responde === 'ไม่มีเลย') { score = 0 ; } 
    else if ( responde === 'เป็นบางวัน') { score = 1 ; }
    else if (responde === 'เป็นบ่อย'){score = 2 ; }
    else if( responde === 'เป็นทุกวัน'){ score = 3 ; }    
    return admin.database().ref(`depressScore/${uid}`).child('total8').set(score);    
}
function depressCalculate9(agent) {
  let responde = request.body.queryResult.parameters.score ;
  let score ;
   if(responde === 'ไม่มีเลย') { score = 0 ; } 
  else if ( responde === 'เป็นบางวัน') { score = 1 ; }
  else if (responde === 'เป็นบ่อย'){score = 2 ; }
  else if( responde === 'เป็นทุกวัน'){ score = 3 ; }    
  return admin.database().ref(`depressScore/${uid}`).child('total9').set(score), scoreTotal() ;
}

  // suicide //
  
  function suicideIntent1(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ใช่') { score =  0 ; } 
     	 else if ( responde === 'ไม่ใช่') { score = 1 ;  }
         return admin.database().ref('suicideScore').child('total1').set(score) ;
  }
function suicideIntent2(agent) {
   	
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ใช่') { score =  0 ; } 
     	 else if ( responde === 'ไม่ใช่') { score = 2 ;  }
         return admin.database().ref('suicideScore').child('total2').set(score) ;

  }
  function suicideIntent3(agent) {
   	         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ใช่') { score =  0 ; } 
     	 else if ( responde === 'ไม่ใช่') { score = 6 ;  }
         return admin.database().ref('suicideScore').child('total3').set(score) ;
	
  }
  function suicideIntent4(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ได้') { score =  0 ; } 
     	 else if ( responde === 'ไม่ได้') { score = 8;  }
         return admin.database().ref('suicideScore').child('total4').set(score)  ;
  }
  function suicideIntent5(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ได้') { score =  0 ; } 
     	 else if ( responde === 'ไม่ได้') { score = 8 ;  }
         return admin.database().ref('suicideScore').child('total5').set(score)  ;
  }
  function suicideIntent6(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ได้') { score = 0 ; } 
     	 else if ( responde === 'ไม่ได้') { score = 9 ;  }
         return admin.database().ref('suicideScore').child('total6').set(score);
  }
  function suicideIntent7(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ได้') { score = 0; } 
     	 else if ( responde === 'ไม่ได้') { score =4 ;  }
         return admin.database().ref('suicideScore').child('total7').set(score) ;
  }
  function suicideIntent8(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ได้') { score = 0 ; } 
     	 else if ( responde === 'ไม่ได้') { score =10;  }
         return admin.database().ref('suicideScore').child('total8').set(score)  ;
  }
  function suicideIntent9(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ได้') { score =  0 ; } 
     	 else if ( responde === 'ไม่ได้') { score = 4 ;  }
         return admin.database().ref('suicideScore').child('total9').set(score), scoreSuicide()  ;
  }
  
  // stress
  function stress1(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'แทบไม่มี') { score = 0 ; } 
   		else if ( responde === 'เป็นบางครั้ง') { score = 1 ; }
    	else if (responde === 'บ่อยครั้ง'){score = 2 ; }
    	else if( responde === 'เป็นประจำ'){ score = 3 ; }    
        return admin.database().ref(`stressScore/${uid}`).child('total1').set(score)  ;
  }
  function stress2(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'แทบไม่มี') { score = 0 ; } 
   		else if ( responde === 'เป็นบางครั้ง') { score = 1 ; }
    	else if (responde === 'บ่อยครั้ง'){score = 2 ; }
    	else if( responde === 'เป็นประจำ'){ score = 3 ; }    
         return admin.database().ref(`stressScore/${uid}`).child('total2').set(score)  ;
  }
  function stress3(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'แทบไม่มี') { score = 0 ; } 
   		 else if ( responde === 'เป็นบางครั้ง') { score = 1 ; }
    	 else if (responde === 'บ่อยครั้ง'){score = 2 ; }
    	 else if( responde === 'เป็นประจำ'){ score = 3 ; }    
         return admin.database().ref(`stressScore/${uid}`).child('total3').set(score);
  }
  function stress4(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'แทบไม่มี') { score = 0 ; } 
   		else if ( responde === 'เป็นบางครั้ง') { score = 1 ; }
    	else if (responde === 'บ่อยครั้ง'){score = 2 ; }
    	else if( responde === 'เป็นประจำ'){ score = 3 ; }    
         return admin.database().ref(`stressScore/${uid}`).child('total4').set(score) ;
  }
  function stress5(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'แทบไม่มี') { score = 0 ; } 
   		else if ( responde === 'เป็นบางครั้ง') { score = 1 ; }
    	else if (responde === 'บ่อยครั้ง'){score = 2 ; }
    	else if( responde === 'เป็นประจำ'){ score = 3 ; }    
         return admin.database().ref(`stressScore/${uid}`).child('total5').set(score), scoreStress()  ;
  }
  
  //Mental Health
  
  function mentalHealth1(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total1').set(score)  ;
  }
   function mentalHealth2(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total2').set(score)  ;
  }
   function mentalHealth3(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 4 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 3 ; }
    	else if (responde === 'มาก'){score = 2 ; }
    	else if( responde === 'มากที่สุด'){ score = 1 ; }    
        return admin.database().ref('happinessScore').child('total3').set(score)  ;
  }
   function mentalHealth4(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 4 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 3 ; }
    	else if (responde === 'มาก'){score = 2 ; }
    	else if( responde === 'มากที่สุด'){ score = 1 ; }    
        return admin.database().ref('happinessScore').child('total4').set(score)  ;
  }
   function mentalHealth5(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 4 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 3 ; }
    	else if (responde === 'มาก'){score = 2 ; }
    	else if( responde === 'มากที่สุด'){ score = 1 ; }    
        return admin.database().ref('happinessScore').child('total5').set(score)  ;
  }

    function mentalHealth6(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total6').set(score)  ;
  }
    function mentalHealth7(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total7').set(score)  ;
  }
    function mentalHealth8(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total8').set(score)  ;
  }
    function mentalHealth9(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total9').set(score)  ;
  }
    function mentalHealth10(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total10').set(score)  ;
  }
    function mentalHealth11(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total11').set(score)  ;
  }
    function mentalHealth12(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total12').set(score)  ;
  }
    function mentalHealth13(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total13').set(score)  ;
  }
    function mentalHealth14(agent) {
         let responde = request.body.queryResult.parameters.score ;
    	 let score;
     	 if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total14').set(score)  ;
  }
    function mentalHealth15(agent) {
      let responde = request.body.queryResult.parameters.score ;
    	let score;
     	if(responde === 'ไม่เลย') { score = 1 ; } 
   		else if ( responde === 'เล็กน้อย') { score = 2 ; }
    	else if (responde === 'มาก'){score = 3 ; }
    	else if( responde === 'มากที่สุด'){ score = 4 ; }    
        return admin.database().ref('happinessScore').child('total15').set(score) , scoreMentalHealth() ;
  }
  
  function peopleInTest(agent) {
      let responde = request.body.queryResult.parameters.score ;
      return admin.database().ref('peopleInTest').child('depression').push(responde)  ;
  }
 
  function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
  }

  // // Uncomment and edit to make your own intent handler
  // // uncomment `intentMap.set('your intent name here', yourFunctionHandler);`
  // // below to get this function to be run when a Dialogflow intent is matched
  // function yourFunctionHandler(agent) {
  //   agent.add(`This message is from Dialogflow's Cloud Functions for Firebase editor!`);
  //   agent.add(new Card({
  //       title: `Title: this is a card title`,
  //       imageUrl: 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png',
  //       text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
  //       buttonText: 'This is a button',
  //       buttonUrl: 'https://assistant.google.com/'
  //     })
  //   );
  //   agent.add(new Suggestion(`Quick Reply`));
  //   agent.add(new Suggestion(`Suggestion`));
  //   agent.setContext({ name: 'weather', lifespan: 2, parameters: { city: 'Rome' }});
  // }

  // // Uncomment and edit to make your own Google Assistant intent handler
  // // uncomment `intentMap.set('your intent name here', googleAssistantHandler);`
  // // below to get this function to be run when a Dialogflow intent is matched
  // function googleAssistantHandler(agent) {
  //   let conv = agent.conv(); // Get Actions on Google library conv instance
  //   conv.ask('Hello from the Actions on Google client library!') // Use Actions on Google library
  //   agent.add(conv); // Add Actions on Google library responses to your agent's response
  // }
  // // See https://github.com/dialogflow/fulfillment-actions-library-nodejs
  // // for a complete Dialogflow fulfillment library Actions on Google client library v2 integration sample

  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  // intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);
  intentMap.set('answer1', peopleInTest);
  intentMap.set('answer2', depressCalculate1);
  intentMap.set('answer3', depressCalculate2);
  intentMap.set('answer4', depressCalculate3);
  intentMap.set('answer5', depressCalculate4);
  intentMap.set('answer6', depressCalculate5);
  intentMap.set('answer7', depressCalculate6); 
  intentMap.set('answer8', depressCalculate7); 
  intentMap.set('answer9', depressCalculate8);
  intentMap.set('finish' ,  depressCalculate9);
  
  intentMap.set('suicide2' ,  suicideIntent1);
  intentMap.set('suicide3' ,  suicideIntent2);
  intentMap.set('suicide4' ,  suicideIntent3);
  intentMap.set('suicide5' ,  suicideIntent4);
  intentMap.set('suicide6' ,  suicideIntent5);
  intentMap.set('suicide7' ,  suicideIntent6);
  intentMap.set('suicide8' ,  suicideIntent7);
  intentMap.set('suicide9' ,  suicideIntent8);
  intentMap.set('finishSuicide' ,  suicideIntent9);
  intentMap.set('stress2' ,  stress1);
  intentMap.set('stress3' ,  stress2);
  intentMap.set('stress4' ,  stress3);
  intentMap.set('stress5' ,  stress4);
  intentMap.set('finishStress' ,  stress5);
  
  intentMap.set('MentalHealth2' ,  mentalHealth1);
  intentMap.set('MentalHealth3' ,  mentalHealth2);
  intentMap.set('MentalHealth4' ,  mentalHealth3);
  intentMap.set('MentalHealth5' ,  mentalHealth4);
   intentMap.set('MentalHealth6' ,  mentalHealth5);
  intentMap.set('MentalHealth7' ,  mentalHealth6);
  intentMap.set('MentalHealth8' ,  mentalHealth7);
  intentMap.set('MentalHealth9' ,  mentalHealth8);
   intentMap.set('MentalHealth10' ,  mentalHealth9);
  intentMap.set('MentalHealth11' ,  mentalHealth10);
  intentMap.set('MentalHealth12' ,  mentalHealth11);
  intentMap.set('MentalHealth13' ,  mentalHealth12);
   intentMap.set('MentalHealth14' ,  mentalHealth13);
  intentMap.set('MentalHealth15' ,  mentalHealth14);
  intentMap.set('finishMentalHealth' ,  mentalHealth15);

  
  
  
  // intentMap.set('your intent name here', yourFunctionHandler);
  // intentMap.set('your intent name here', googleAssistantHandler);
  agent.handleRequest(intentMap);
});