import { Component, OnInit } from '@angular/core';
import { FirebaseService } from './../services/firebase.service';
import { ActivatedRoute } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import { Router } from '@angular/router';

declare var google ;
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {

  
  paramlat = this.route.snapshot.paramMap.get('lat');
  paramlng = this.route.snapshot.paramMap.get('lng');
  map = null ;
  locationList;
  lat = 13.7649082;
  lng = 100.5295513;
  icon = { url:"./assets/pin.png",
  scaledSize: {
    width: 90,
    height: 90
  }
}
  constructor(
    private firebase: FirebaseService, 
    private route:ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit() {
    // this.loadMap();
    this.getLocation();
    if(this.paramlat){
      let realLat = parseFloat(this.paramlat);
      let realLng = parseFloat(this.paramlng)
      console.log(realLat, realLng);
      this.lat= realLat ,
      this.lng = realLng
      this.icon = { url:"./assets/pin.png",
      scaledSize: {
      width: 80,
      height: 80
    }
 }
    }
 }
 

 getLocation() {
  this.firebase.getMap().subscribe(data => {
    this.locationList = data.map(e => {
      return {
        key: e.key,
        value: e.payload.val()
      } as unknown as Marker;
    });
    console.log(this.locationList) ;
    
  });
}
goHospital(item){
   console.log( `hospital: ${item.value.locationName}`,item.value.latitude ,item.value.longitude);
   this.lat = item.value.latitude;
   this.lng = item.value.longitude;

   
}

 loadMap() {
   // create a new map by passing HTMLElement
   const mapEle: HTMLElement = document.getElementById('map');
   // create LatLng object
   const myLatLng = {lat: this.lat, lng: this.lng };
   // create map
   this.map = new google.maps.Map(mapEle, {
     center: myLatLng,
     zoom: 15
   });
   google.maps.event.addListenerOnce(this.map, 'idle', () => {
     new google.maps.Marker({
           position: myLatLng ,
           map : this.map ,
         })  
     mapEle.classList.add('show-map');
   });
 }

 back(){
    this.router.navigate(['/tabs/tab1'])
 }

}
