import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
// import { CalendarComponentOptions } from 'ion2-calendar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';
import { CalendarComponentOptions } from 'ion2-calendar';

@Component({
  selector: 'app-calendar-dialog',
  templateUrl: './calendar-dialog.component.html',
  styleUrls: ['./calendar-dialog.component.scss'],
})
export class CalendarDialogComponent implements OnInit  {
  private domSanitize: DomSanitizer
  src;
  constructor(private modalCtrl: ModalController ,
    private dialogRef: MatDialogRef<CalendarDialogComponent, any>,
    @Inject(MAT_DIALOG_DATA) public data: any
     ) { }

  ngOnInit() {
    console.log(this.data);
    
  }
  
  sanitize(url) {
    return this.domSanitize.bypassSecurityTrustUrl(url);
  }


}
