import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { MapComponent } from './map/map.component';
import { AgmCoreModule } from '@agm/core';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ReadNewsComponent } from './tab3/components/read-news/read-news.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbChatModule, NbSpinnerModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { HttpClientModule } from '@angular/common/http';
import { ChatComponent } from './chat/chat.component';
import { DiaryPage1Component } from './diary-page1/diary-page1.component';
import { NgCalendarModule  } from 'ionic2-calendar';
// Calendar UI Module
// import { CalendarModule } from 'ion2-calendar';
//File
import {Media} from '@ionic-native/media/ngx'
import {File} from '@ionic-native/file/ngx'
import { DiarySecondComponent } from './diary-second/diary-second.component';
import { MoodStatComponent } from './mood-stat/mood-stat.component';
import { SettingComponent } from './setting/setting.component';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { GoogleChartsModule } from 'angular-google-charts';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { CommonModule } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { CalendarDialogComponent } from './calendar-dialog/calendar-dialog.component';
import { DocumentComponent } from './document/document.component';
import { CalendarModule } from 'ion2-calendar';
import { SettingPassComponent } from './setting-pass/setting-pass.component';
import { TabsPage } from './tabs/tabs.page';
import { AudiorecordComponent } from './audiorecord/audiorecord.component';
import { StressComponent } from './stress/stress.component';
import { SuicideComponent } from './suicide/suicide.component';
import { DepressionComponent } from './depression/depression.component';
import { HappyComponent } from './happy/happy.component';
import { TesthistoryComponent } from './testhistory/testhistory.component';
import { TestwarningDialogComponent } from './testwarning-dialog/testwarning-dialog.component';
import { SnackbarInvalidComponent } from './snackbar-invalid/snackbar-invalid.component';
@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    LoginComponent,
    SignUpComponent,
    ChatComponent,
    DiaryPage1Component,
    DiarySecondComponent ,
    MoodStatComponent,
    SettingComponent ,
    CalendarDialogComponent,
    DocumentComponent,
    SettingPassComponent,
    AudiorecordComponent,
    StressComponent,
    SuicideComponent,
    DepressionComponent,
    HappyComponent,
    TesthistoryComponent,
    TestwarningDialogComponent,
    SnackbarInvalidComponent
  ],
  entryComponents: [CalendarDialogComponent , SettingPassComponent,TestwarningDialogComponent , SnackbarInvalidComponent],
  imports: [
    CommonModule,
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCqBau3CoAgOrUUux7iwl8Doz6VhcPVk04',
    }),
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgCalendarModule,
    CalendarModule ,
    // Ng2GoogleChartsModule,
    GoogleChartsModule,
    AngularMaterialModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Media,
    File
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
