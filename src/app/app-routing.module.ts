import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { MapComponent } from './map/map.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ReadNewsComponent } from './tab3/components/read-news/read-news.component';
import { ChatComponent } from './chat/chat.component';
import { AuthService } from './auth/auth.service';
import { DiaryPage1Component } from './diary-page1/diary-page1.component';
import { DiarySecondComponent } from './diary-second/diary-second.component';
import { MoodStatComponent } from './mood-stat/mood-stat.component';
import { SettingComponent } from './setting/setting.component';
import { DocumentComponent } from './document/document.component';
import { AuthGuard } from './guard/auth.guard';
import { AudiorecordComponent } from './audiorecord/audiorecord.component';
import { StressComponent } from './stress/stress.component';
import { HappyComponent } from './happy/happy.component';
import { DepressionComponent } from './depression/depression.component';
import { SuicideComponent } from './suicide/suicide.component';
import { TesthistoryComponent } from './testhistory/testhistory.component';
// import { AuthService } from './../auth/auth.service';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule) ,
    canActivate: [AuthGuard]  
  },
  {
    path: 'map' , component: MapComponent ,  
  },
  {
    path: 'maping/:lat/:lng' , component: MapComponent ,  
  },
  {
    path: 'login' , component: LoginComponent
  },
  {
    path: 'signup' , component: SignUpComponent
  },
  {
    path: 'chatbot/:id' , component: ChatComponent
  },
  {
    path: 'diaryFirst' , component : DiaryPage1Component 
  },
  {
    path: 'diarysecond' , component : DiarySecondComponent 
  },
  {
    path: 'moodStatic' , component : MoodStatComponent 
  },
  {
    path: 'setting' , component: SettingComponent
  },
  {
    path: 'document' , component: DocumentComponent
  },
  {
    path: 'recording' , component: AudiorecordComponent
  },
  {
    path: 'stress' , component: StressComponent
  },
  {
    path: 'happy' , component: HappyComponent
  },
  {
    path: 'depression' , component: DepressionComponent
  },
  {
    path: 'suicide' , component: SuicideComponent
  },
  {
    path: 'history' , component: TesthistoryComponent
  }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
