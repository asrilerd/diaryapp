import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-depression',
  templateUrl: './depression.component.html',
  styleUrls: ['./depression.component.scss'],
})
export class DepressionComponent implements OnInit {
  quesion;
  quesionArr = [] ;
  ansArr = [];
  Qindex = 1;
  qArrLenght;
  date = new Date();
  currentDate= this.date.valueOf();
  total=0;
  depressResult;
  constructor(private firebase: FirebaseService , private router: Router) { }
  profile
  totalcard = false;

  public userLogged = JSON.parse(localStorage.getItem('user'));
  ngOnInit() {
    this.getQuesion();
    this.getAns();
    this.getUserByID();
    setTimeout(() => {
      this.quesionArr.map((r) => {
        this.qArrLenght = r.length;
        // console.log(this.qArrLenght);
      });
    }, 500);
  }
  getQuesion() {
    this.firebase.depressQ().subscribe((data) => {
      let x = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      this.quesionArr.push(x);
      console.log(this.quesionArr);
    });
  }
  getAns() {
    this.firebase.depressAns().subscribe((data) => {
      let x = data.map((e) => {
        e.payload.ref.on('value',snap => {
          this.ansArr.push(snap.val());
          console.log(snap.val());
        })
      });
    });
  }
  getUserByID(){
    this.firebase.getProfile(this.userLogged.uid).subscribe(
      data=> {
         data.map(item => {
          this.profile = item.payload.val();
         })
         console.log(this.profile);
         
      }
    )
  }

  calScore(score,event){
    if (this.Qindex < this.qArrLenght) {
      this.Qindex++;
      this.total += score
      // console.log('index',this.Qindex);
      console.log('score',score);
      if (this.total < 7) {
        this.depressResult = 'คุณไม่มีภาวะซึมเศร้า'
      } else if (this.total >= 7 && this.total <= 12) {
        this.depressResult = 'คุณมีภาวะซึมเศร้าระดับน้อย'
      } else if (this.total >= 13 && this.total <= 19) {
          this.depressResult = 'คุณมีภาวะซึมเศร้าระดับกลาง'
      } else if (this.total > 19) {
          this.depressResult = 'คุณมีภาวะซึมเศร้าระดับรุนแรง'
      }
    } else {
      event.preventDefault(); 
    }
  }
  submit(){
    console.log("คะแนน: ",this.total);
    document.getElementById('send').style.display = "none"
    this.totalcard = true;
    let data = {
       totalScore : this.total,
       gender : this.profile.gender,
       result:this.depressResult,
       completeDT : this.currentDate,
       user:this.profile.user
    }
    console.log(data);
   
    this.firebase.depressSubmit(data,this.userLogged.uid);
  }

  backtoMain(){
    this.router.navigate(['/tabs/tab1'])
  }

}
