import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import { Tab3Page } from '../tab3/tab3.page';
import { NbThemeModule, NbChatModule, NbSpinnerModule, NbLayoutModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { AngularMaterialModule } from '../angular-material/angular-material.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    AngularMaterialModule
   
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
