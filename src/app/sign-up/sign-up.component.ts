import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseService } from './../services/firebase.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarInvalidComponent } from '../snackbar-invalid/snackbar-invalid.component';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {

  form: FormGroup;
  get fname (){
    return this.form.get('firstname');
  }
  get lname (){
    return this.form.get('lastname');
  }
  get emailuser (){
    return this.form.get('email');
  }
  get pass (){
    return this.form.get('password');
  }
  get careers (){
    return this.form.get('career');
  }
  get edu (){
    return this.form.get('education');
  }
  get maritial (){
    return this.form.get('maritialStatus');
  }
  get age (){
    return this.form.get('age');
  }
  get phone (){
    return this.form.get('phone');
  }
  error
  career:string[] = ['นักเรียน/นักศึกษา','ข้าราชการ','รัฐวิสาหกิจ','เจ้าของกิจการ','ลูกจ้าง/พนักงาน','ว่างงาน'];
  education:string[] = ['มัธยมต้น','มัธยมปลายหรือเทียบเท่า','ปริญญาตรี','อนุปริญญาตรี','ปริญญาโท','สูงกว่าปริญญาโท'];
  status:string[] = ['โสด','แต่งงาน','หย่าร้าง','แยกกันอยู่'];
  
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private firebase: FirebaseService,
    private snackbar : MatSnackBar,
    public toastController: ToastController,
    private router:Router
  ) { }

  ngOnInit() {
    this.initform();
  }
  private initform(){
    this.form = this.fb.group({
      firstname: [ null ,Validators.required],
      lastname: [ null ,Validators.required],
      user:[ null ,Validators.required],
      email: [ null ,Validators.required, Validators.email] ,
      password: [ null ,Validators.required],
      // phone:[ null ,Validators.required],
      gender:[ null ,Validators.required] ,
      age:[ null ,Validators.required],
      career:[ null ,Validators.required],
      education:[ null ,Validators.required],
      maritialStatus:[ null ,Validators.required],
      role:"user",
    })  
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'กรอกข้อมูลให้ครบ',
      duration: 2000
    });
    toast.present();
  }

  Register(){
    if(this.form.invalid){
      this.presentToast()
    } else{
      console.log('add', this.form.value);
      this.auth.SignUp(this.emailuser.value,this.pass.value , this.form.value);
      this.form.reset();
      this.router.navigate(['login'])
    }
  }
  getErrorMessage() {
    if (this.emailuser.hasError('required')) {
      return 'กรุณากรอกข้อมูล'
    } else if(this.emailuser.hasError('email')) {
      return 'ไม่ใช่รูปแบบของอีเมล';
    } 
  }
 ///เขียนerrorเป็นเมธอด แล้วรีเทริ์นทีเดียว 
}
