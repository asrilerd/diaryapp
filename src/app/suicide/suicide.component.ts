import { Component, OnInit } from "@angular/core";
import { FirebaseService } from "../services/firebase.service";
import { async } from '@angular/core/testing';
import { Router } from '@angular/router';

@Component({
  selector: "app-suicide",
  templateUrl: "./suicide.component.html",
  styleUrls: ["./suicide.component.scss"],
})
export class SuicideComponent implements OnInit {
  quesion;
  quesionArr = [];
  ansArr = [];
  arrMinor = [];
  Qindex: number = 0;
  qArrLenght: number;
  date = new Date();
  currentDate = this.date.valueOf();
  total = 0;
  index = 1;
  suicideResult: string;
  profile
  totalcard = false
  constructor(private firebase: FirebaseService , private router : Router) {}

  public userLogged = JSON.parse(localStorage.getItem("user"));
  ngOnInit() {
    this.getQuesion();
    this.getAns();
    this.getUserByID();
    setTimeout(() => {
      this.quesionArr.map((r) => {
        this.qArrLenght = r.length;
        // console.log(this.qArrLenght);
      });
    }, 0);
  }
  getQuesion() {
    this.firebase.suicideQ().subscribe((data) => {
      let x = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      this.quesionArr.push(x);
      // console.log(this.quesionArr);
    });
  }
  getAns() {
    this.firebase.suicideAns().subscribe((data) => {
      let x = data.map((e) => {
        e.payload.ref.child("answer").on("value", (snap) => {
          this.ansArr.push(snap.val());
          // console.log("array คำตอบ",this.ansArr);
          this.anstoArr(0);
        });
      });
    });
  }
  anstoArr(index) {
    this.arrMinor.shift();
    this.arrMinor.push(this.ansArr[index]);
    // console.log(this.arrMinor);
  }

  calScore(score, event) {
    if (this.Qindex < this.qArrLenght) {
      this.Qindex++;
      this.total += score;
      // console.log('index',this.Qindex);
      console.log("score", score);
      this.anstoArr(this.index);
      this.index++;
      if (this.total === 0) {
        this.suicideResult = 'คุณไม่มีภาวะซึมเศร้า';
      } else if (this.total >= 1 && this.total <= 8) {
        this.suicideResult = 'แนวโน้มฆ่าตัวตายเล็กน้อย';
      } else if (this.total >= 9 && this.total <= 16) {
        this.suicideResult = 'แนวโน้มฆ่าตัวตายระดับปานกลาง';
      } else if (this.total > 17) {
        this.suicideResult = 'แนวโน้มฆ่าตัวตายระดับรุนแรง';
      }
    } else {
      event.preventDefault();
    }
  }

  getUserByID(){
    this.firebase.getProfile(this.userLogged.uid).subscribe(
      data=> {
         data.map(item => {
          this.profile = item.payload.val();
         })
         console.log(this.profile);
         
      }
    )
  }
  submit() {
    this.totalcard = true
    console.log("คะแนน: ", this.total);
    document.getElementById('send').style.display = "none"
    let data = {
      totalScore: this.total,
      gender: this.profile.gender,
      result: this.suicideResult,
      completeDT: this.currentDate,
      user:this.profile.user,
    };
    console.log(data);
    this.firebase.suicideSubmit(data,this.userLogged.uid);
  }
  
  backtoMain(){
    this.router.navigate(['/tabs/tab1'])
  }
}
