import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth/auth.service';
import { FirebaseService } from '../services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  img=["./assets/sad.png" ,"./assets/empathy.png" ,"./assets/icon/love-and-romance.png" ,"./assets/icon/statistics.png" ,"./assets/icon/newspaper.png","./assets/icon/hospital.png" ]
  newsList: any[];
  docList:any[];
  userLogged = JSON.parse(localStorage.getItem("user"));
  score:number = 0 ; 
  text:string ;
  imgicon;
  date=new Date();
  currentDate = this.date.toDateString()
  username;
  x
  sliderConfig = {
    spaceBetween:7 ,
    centeredSlides:false,
    slidesPerView:1.9
  } // คอนฟิก ตั้งค่า สไลด์
  constructor( 
    public auth : AuthService , 
    private firebase:FirebaseService,
    private router: Router) {}

  ngOnInit() {
    this.showMyNews();
    this.file();
    this.getScoreIcon();
    this.getUserByUid()
  }
  getUserByUid(){
    this.firebase.getProfile(this.userLogged.uid).subscribe(
      data => {
        data.map( item =>{
           let username:any = item.payload.val()
           this.username = username?.user
        })
      }
    )
  }
 
  diary(){
    this.router.navigate(['/diaryFirst'])
  }
  showMyNews(){
    this.firebase.getNewsAd().subscribe( data => {
         this.newsList = data.map( e=> {
           return {
             key : e.key ,
             value : e.payload.val()
           }
         });
         console.log(this.newsList);
         
    });
  }
  file(){
    this.firebase.getDoc().subscribe( data => {
         this.docList = data.map( e=> {
           return {
             key : e.key ,
             value : e.payload.val()
           }
         });
        //  console.log(this.docList);
         
    });
  }
  route(item){
    console.log(item);
    if(item === "MentalHealth"){
      const uid = this.auth.userData.uid
      this.router.navigate([`chatbot/${uid}`])
    } else if(item === "News"){
      this.router.navigate(['tabs/tab3'])
    }else if(item === "Hospital"){
      this.router.navigate(['map'])
    }else if(item === "Diary"){
      this.router.navigate(['diarysecond'])
    }else if(item === "Mood Static"){
      this.router.navigate(['moodStatic'])
    }else if(item === "Doc"){
      this.router.navigate(['document'])
    }else if(item === "history"){
      this.router.navigate(['history'])
    }
  }
  news(){
    this.router.navigate(['tabs/tab3'])
  }

  getScoreIcon() {
    this.firebase
      .getscore(this.userLogged.uid)
      .subscribe((data) => {
        // console.log("subscribeData :", data);
        this.score = 0;
        if (data.length === 0) {
        } else {
          data.forEach((item:item) => {
            
            this.score = this.score + item.icon.iconScore;
          });
         
          // console.log(data.length);
          // console.log(this.score);
          this.score = this.score /data.length
          if(this.score <4){
            this.x=0
          } else if(this.score >4){
            this.x=1
          } 
          this.sentence();
        }
      });
  }

  sentence(){
    if(this.score === 0){
       this.text = "สุขภาพจิตของคุณค่อนข้างไม่ดีนะ "
    } else if (this.score >= 1 && this.score < 4){
      this.text = "มีเรื่องกังวลใจอะไรหรือเปล่า "
    } else if (this.score >= 4 && this.score < 6){
      this.text = "ช่วงนี้คุณดูสดใสขึ้นนะ"
    } else if (this.score >= 6 && this.score < 7){
      this.text = "ช่วงนี้อารมณ์ดีใช่ไหม~"
    } else if (this.score >= 7 && this.score < 8){
      this.text = "มีความสุขกันเยอะๆนะ"
    } else if (this.score >= 8 && this.score <= 9){
      this.text = "ยิ้มเยอะๆนะ"
    }

  }
}
