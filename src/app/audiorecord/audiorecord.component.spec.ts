import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AudiorecordComponent } from './audiorecord.component';

describe('AudiorecordComponent', () => {
  let component: AudiorecordComponent;
  let fixture: ComponentFixture<AudiorecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudiorecordComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AudiorecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
