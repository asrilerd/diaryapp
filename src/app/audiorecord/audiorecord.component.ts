import { Component, OnInit } from "@angular/core";
import { AngularFireStorage } from "@angular/fire/storage";
import { MediaObject } from "@ionic-native/media/ngx";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import * as RecordRTC from "recordrtc";
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: "app-audiorecord",
  templateUrl: "./audiorecord.component.html",
  styleUrls: ["./audiorecord.component.scss"],
})
export class AudiorecordComponent implements OnInit {
  status: string = null;
  audioFile: MediaObject;
  downloadURL: Observable<string>;

  //Lets initiate Record OBJ
  record;
  //Will use this flag for detect recording
  recording = false;
  //Url of Blob
  url;
  private error;
  public userLogged = JSON.parse(localStorage.getItem("user"));
  constructor(private storage: AngularFireStorage,private domSanitize: DomSanitizer,) {}

  ngOnInit() {}
  initiateRecording() {
    this.status = "recording...";
    this.recording = true;
    let mediaConstraints = {
      video: false,
      audio: true,
    };
    navigator.mediaDevices
      .getUserMedia(mediaConstraints)
      .then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }
  /**
   * Will be called automatically.
   */
  successCallback(stream) {
    var options = {
      mimeType: "audio/wav",
      numberOfAudioChannels: 1,
    };
    //Start Actuall Recording
    var StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.record = new StereoAudioRecorder(stream, options);
    this.record.record();
  }
  /**
   * Stop recording.
   */
  stopRecording() {
    this.status = "stopped";
    this.recording = false;
    this.record.stop(this.processRecording.bind(this));
  }
  /**
   * processRecording Do what ever you want with blob
   * @param  {any} blob Blog
   */
  processRecording(blob) {
    this.url = URL.createObjectURL(blob);
    console.log(this.url);

    this.onFileSelected(blob);
  }
  /**
   * Process Error.
   */
  errorCallback(error) {
    this.error = "Can not play audio in your browser";
  }
 
  onFileSelected(event) {
    var n = Date.now();
    const file = event;
    const filePath = `voice/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`voice/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(async () => {
          const uid = this.userLogged.uid;
          this.downloadURL = await fileRef.getDownloadURL().toPromise();
          // this.db.database.ref(`Diary/${uid}`).push(this.downloadURL);
          return (
            // this.voiceUrl.setValue(this.downloadURL),
            // this.voicePath.setValue(filePath),
            console.log("path :", filePath, "url :", this.downloadURL)
          );
        })
      )
      .subscribe((url) => {
        if (url) {
          console.log(url, "uploaded..");
        }
      });
  }
  sanitize(url) {
    return this.domSanitize.bypassSecurityTrustUrl(url);
  }

}
