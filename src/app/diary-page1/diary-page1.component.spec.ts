import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiaryPage1Component } from './diary-page1.component';

describe('DiaryPage1Component', () => {
  let component: DiaryPage1Component;
  let fixture: ComponentFixture<DiaryPage1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiaryPage1Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiaryPage1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
