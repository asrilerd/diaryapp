import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from "@angular/forms";
import { FirebaseService } from "src/app/services/firebase.service";
// import { CalendarComponentOptions } from "ion2-calendar";
import { title } from "process";
import { AuthService } from "./../auth/auth.service";
import { map, finalize } from "rxjs/operators";
import { Media, MediaObject } from "@ionic-native/media/ngx";
import { File } from "@ionic-native/file/ngx";
import { AngularFireDatabase } from "@angular/fire/database";
import { Router } from "@angular/router";
import {
  NavController,
  AlertController,
  ModalController,
} from "@ionic/angular";
import { async } from "@angular/core/testing";
import { DomSanitizer } from "@angular/platform-browser";
import * as RecordRTC from "recordrtc";
import { AngularFireStorage } from "@angular/fire/storage";
import { Observable } from "rxjs";
import { AudiorecordComponent } from "../audiorecord/audiorecord.component";
@Component({
  selector: "app-diary-page1",
  templateUrl: "./diary-page1.component.html",
  styleUrls: ["./diary-page1.component.scss"],
})
export class DiaryPage1Component implements OnInit {
  status: string = null;
  audioFile: MediaObject;
  form: FormGroup;

  icon = [
    "โกรธ",
    "เศร้า",
    "เหงา",
    "แย่",
    "เบื่อ",
    "เหนื่อย",
    "ชิล",
    "มีความสุข",
  ];
  iconShow=['./assets/icon/051-angry.png',
  './assets/icon/051-cry.png',
  './assets/icon/051-sad.png',
  './assets/icon/051-sad-1.png',
  './assets/icon/051-unamused.png',
  './assets/icon/051-tired.png',
  './assets/icon/051-smile-1.png',
  './assets/icon/051-smiling.png']
  indexIcon = 0;
  activity = ["restaurant", "football", "bed"];
  card = false;
  date: string;
  type: "string";
  eventSource = [];
  dateTime = new Date();
  currentDate = this.dateTime.toLocaleDateString();
  currentTime = this.dateTime.toLocaleTimeString();
  timeStamp = this.dateTime.valueOf();
  diary;
  public userLogged = JSON.parse(localStorage.getItem("user"));
  iconName;
  iconChip = ["game-controller", "football", "bed"];
  activityarr = [];
  downloadURL: Observable<string>;
  range = 0;
  //Lets initiate Record OBJ
  record;
  //Will use this flag for detect recording
  recording = false;
  //Url of Blob
  url;
  private error;
  slide1 = true;
  slide2 = false;
  slide3 = false;
  iconAct= ['game-controller','bed','walk','car-sport','desktop','book','heart-outline','bicycle','home']
  get addicon() {
    return this.form.get("icon");
  }

  get addActivity() {
    return this.form.get("activity");
  }
  get voicePath() {
    return this.form.get("voicePath");
  }
  get voiceUrl() {
    return this.form.get("voiceUrl");
  }
  get lat() {
    return this.form.get("lat");
  }
  get lng() {
    return this.form.get("lng");
  }
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    centeredSlides: false,
  };
  checks
  // public checks: Array<any> = [
  //   {description: 'เรียน', value: 'เรียน'},
  //   {description: "ทำงาน", value: 'ทำงาน'},
  //   {description: "นอนหลับ", value: 'นอนหลับ'},
  //   {description: "เล่นเกม", value: 'เล่นเกม'},
  //   {description: "ออกไปข้างนอก", value: 'ออกไปข้างนอก'},
  //   {description: "ไปเที่ยว", value: 'ไปเที่ยว'}
  // ];
  

  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private auth: AuthService,
    private media: Media,
    private file: File,
    private db: AngularFireDatabase,
    private router: Router,
    public navCtrl: NavController,
    public alertController: AlertController,
    private domSanitize: DomSanitizer,
    private storage: AngularFireStorage,
    public modalController: ModalController
  ) {}

  private initForm() {
    this.form = this.fb.group({
      icon: [null, [Validators.required]],
      activity: new FormArray([]),
      uid: [this.userLogged.uid],
      email: [this.userLogged.email],
      date: [this.currentDate],
      time: [this.currentTime],
      timeStamp: [this.timeStamp],
      text: [null],
      voicePath: [null],
      voiceUrl: [null],
      lat:[null],
      lng:[null]
    });
  }
  ngOnInit() {
    this.initForm();
    this.addicon.setValue({ iconName: "angry", iconScore: 1 });
    // this.getDiary()
    // this.actSelect()
    this.getActivity();
     this.findMe();
  }
  getActivity(){
    this.firebase.getActivity().subscribe( data => {
    this.checks =  data.map( e => {
        return {
          key: e.key , 
          description : e.payload.val()
        }
      })
      console.log(this.checks);
    })
  }

  moodRange(rate) {
    if (rate == 0) {
      this.addicon.setValue({ iconName: "angry", iconScore: 1 });
      console.log( this.addicon.value);
      this.indexIcon=0;
      document.getElementById("ion-padding").style.backgroundColor = "#FC4C59";
    } else if (rate == 1) {
      this.indexIcon=1;
      this.addicon.setValue({ iconName: "sad", iconScore: 2 });
      console.log( this.addicon.value);
      document.getElementById("ion-padding").style.backgroundColor = "#6996AD	";
    } else if (rate == 2) {
      this.indexIcon=2;
      this.addicon.setValue({ iconName: "lonely", iconScore: 3 });
      console.log( this.addicon.value);
      document.getElementById("ion-padding").style.backgroundColor = "#AAAAFF	";
    } else if (rate == 3) {
      this.indexIcon=3;
      this.addicon.setValue({ iconName: "blue", iconScore: 4 });
      console.log( this.addicon.value);
      document.getElementById("ion-padding").style.backgroundColor = "#7AC5CD";
    } else if (rate == 4) {
      this.indexIcon=4;
      this.addicon.setValue({ iconName: "bored", iconScore: 5 });
      console.log( this.addicon.value);
      document.getElementById("ion-padding").style.backgroundColor = "#32CD99";
    } else if (rate == 5) {
      this.indexIcon=5;
      this.addicon.setValue({ iconName: "tired", iconScore: 6 });
      console.log( this.addicon.value);
      document.getElementById("ion-padding").style.backgroundColor = "#C2E2EC";
    } else if (rate == 6) {
      this.indexIcon=6;
      this.addicon.setValue({ iconName: "chill", iconScore: 8 });
      console.log( this.addicon.value);
      document.getElementById("ion-padding").style.backgroundColor = "#FFCD72		";
    } else if (rate == 7) {
      this.indexIcon=7;
      this.addicon.setValue({ iconName: "happy", iconScore: 9 });
      console.log( this.addicon.value);
      document.getElementById("ion-padding").style.backgroundColor = "#F4ADAE";
    }
  }
  onCheckChange(event) {
    const formArray: FormArray = this.form.get('activity') as FormArray;
  
    /* Selected */
    if(event.target.checked){
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
      console.log(formArray);
      
    }
    /* unselected */
    else{
      // find the unselected element
      let i: number = 0;
  
      formArray.controls.forEach((ctrl: FormControl) => {
        if(ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }
  
        i++;
      });
    }
  }

  next() {
    this.card = !this.card;
    console.log(this.card);
  }

  goslide2() {
    this.slide1 = false;
    this.slide3 = false;
    this.slide2 = true;
    document.getElementById("ion-padding").style.backgroundColor = "#F6FAFF";
  }
  goslide3() {
    this.slide1 = false;
    this.slide3 = true;
    this.slide2 = false;
    document.getElementById("ion-padding").style.backgroundColor = "white";

  }
  prevslide1() {
    this.slide1 = true;
    this.slide3 = false;
    this.slide2 = false;
    document.getElementById("ion-padding").style.backgroundColor = "#FC4C59";
  }
  prevslide2() {
    this.slide1 = false;
    this.slide3 = false;
    this.slide2 = true;
    document.getElementById("ion-padding").style.backgroundColor = "#F6FAFF";
  }
  prevslide3() {
    this.slide1 = false;
    this.slide3 = false;
    this.slide2 = true;
    document.getElementById("ion-padding").style.backgroundColor = "white";

  }

  back() {
    this.router.navigate(["tabs/tab1"]);
  }

  actSelect(act) {
    console.log(act);
    this.activityarr.push(act);
    console.log("activityArray", this.activityarr);
    this.addActivity.setValue(this.activityarr);
  }
  findMe() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
         console.log(position);
         this.lat.setValue(position.coords.latitude)  
         this.lng.setValue(position.coords.longitude)  
         console.log(this.lat.value,this.lng.value);
         
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }
  submit() {
    if (this.form.valid) {
      console.log("submit", this.form.value);
      this.firebase.addDiary(this.form, this.userLogged.uid);
      this.form.reset();
      this.router.navigate(["diarysecond"]);
    } else {
      this.showAlert();
    }
  }

  async showAlert() {
    const alert = await this.alertController.create({
      cssClass: "alertClass",
      header: "การบันทึกไม่สำเร็จ",
      message: "กรุณาเลือก icon และพิมพ์ข้อความ",
      buttons: ["OK"],
    });

    await alert.present();
  }

  // startRecording() {
  //   try {
  //     let media = new MediaPlugin('../Library/NoCloud/recording.wav');
  //   media.startRecord();
  //   } catch (error) {
  //     this.showAlert('Could not start recording.');
  //   }

  // }

  sanitize(url) {
    return this.domSanitize.bypassSecurityTrustUrl(url);
  }

  initiateRecording() {
    this.next();
    this.status = "recording...";
    this.recording = true;
    let mediaConstraints = {
      video: false,
      audio: true,
    };
    navigator.mediaDevices
      .getUserMedia(mediaConstraints)
      .then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }
  /**
   * Will be called automatically.
   */
  successCallback(stream) {
    var options = {
      mimeType: "audio/wav",
      numberOfAudioChannels: 1,
    };
    //Start Actuall Recording
    var StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.record = new StereoAudioRecorder(stream, options);
    this.record.record();
  }
  /**
   * Stop recording.
   */
  stopRecording() {
    this.status = "stopped";
    this.recording = false;
    this.record.stop(this.processRecording.bind(this));
  }
  /**
   * processRecording Do what ever you want with blob
   * @param  {any} blob Blog
   */
  processRecording(blob) {
    this.url = URL.createObjectURL(blob);
    console.log(this.url);

    this.onFileSelected(blob);
  }
  /**
   * Process Error.
   */
  errorCallback(error) {
    this.error = "Can not play audio in your browser";
  }
  iconMood(iconName) {
    console.log(iconName);
    this.addicon.setValue({ iconName: iconName, iconScore: 5 });
  }

  onFileSelected(event) {
    var n = Date.now();
    const file = event;
    const filePath = `voice/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`voice/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(async () => {
          const uid = this.userLogged.uid;
          this.downloadURL = await fileRef.getDownloadURL().toPromise();
          // this.db.database.ref(`Diary/${uid}`).push(this.downloadURL);
          return (
            this.voiceUrl.setValue(this.downloadURL),
            this.voicePath.setValue(filePath),
            console.log("path :", filePath, "url :", this.downloadURL)
          );
        })
      )
      .subscribe((url) => {
        if (url) {
          console.log(url, "uploaded..");
        }
      });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AudiorecordComponent,
      cssClass: "my-custom-class",
    });
    return await modal.present();
  }

  backtoDiary(){
    this.router.navigate(['/diarysecond'])
  }

}
