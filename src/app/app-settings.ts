import { Injectable } from "@angular/core";
import { MatDialogConfig } from '@angular/material/dialog';

@Injectable()
export class AppSettings {

  
  static dialogConfig: MatDialogConfig = {
    panelClass: 'app-dialog',
    maxHeight: 'calc(100vh - 32px)',
    width: 'calc(100% - 32px)',
    maxWidth: '500px',
  };

}