import { Component, Inject, OnInit } from '@angular/core';
import { inject } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-setting-pass',
  templateUrl: './setting-pass.component.html',
  styleUrls: ['./setting-pass.component.scss'],
})
export class SettingPassComponent implements OnInit {
  form: FormGroup;
  password:number;
  key;
  constructor(
    private fb : FormBuilder ,
    private db : FirebaseService,
    private dialogRef: MatDialogRef<SettingPassComponent, any>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }
  ngOnInit() {
    this.getProfile();
    console.log(this.data.uid);
    this.initform();
  }
  
  getProfile(){
    this.db.getProfile(this.data.uid).subscribe( data => {
      data.map( item => {
       const i:any =  item.payload.val();
       const password = i.password;
       this.password  = password;
       this.form.get('email').patchValue(this.data.email);
       this.form.get('password').patchValue(this.password);
       this.key = item.key ;
       console.log(this.key);
       
      });
    });
  }
  private initform(){
    this.form = this.fb.group({
      email: null,
      password: null,
      newPassword: null
    })  
   
  }
  update(){
    console.log(this.form.value);
    const newPass = this.form.get('newPassword').value
    this.db.updateUser(this.key,{password:newPass})
    this.dialogRef.close()
  }
}
