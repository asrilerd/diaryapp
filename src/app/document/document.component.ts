import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss'],
})
export class DocumentComponent implements OnInit {

  constructor(private firebase:FirebaseService , ) { }
  doclist:any ;
  ngOnInit() {
    this.getDocument();
  }
  
  getDocument(){
    this.firebase.getDoc().subscribe( (data) => {
      this.doclist = data.map( (item) => {
        return {
          key : item.key ,
          value : item.payload.val()
        }
      });
      console.log(this.doclist);
    }); 
  }
  download(item) {
    console.log(item.value.imageUrl);
    window.open(item.value.imageUrl);
  }
}
