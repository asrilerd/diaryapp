import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { dateFieldName } from '@progress/kendo-angular-intl';
import { database } from 'firebase';
import { FirebaseService } from "src/app/services/firebase.service";

@Component({
  selector: "app-stress",
  templateUrl: "./stress.component.html",
  styleUrls: ["./stress.component.scss"],
})
export class StressComponent implements OnInit {
  quesion;
  quesionArr = [];
  ansArr = [];
  Qindex = 1;
  qArrLenght;
  date = new Date();
  currentDate= this.date.valueOf();
  total=0;
  stressResult;
  profile
  totalcard = false;
  constructor(private firebase: FirebaseService , private router : Router) {}
  public userLogged = JSON.parse(localStorage.getItem('user'));
  ngOnInit() {
    this.getQuesion();
    this.getUserByID();
    this.getAns();
    setTimeout(() => {
      this.quesionArr.map((r) => {
        this.qArrLenght = r.length;
        // console.log(this.qArrLenght);
      });
    }, 0);
  }
  getQuesion() {
    this.firebase.stressQ().subscribe((data) => {
      let x = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      // console.log(this.quesion);
      this.quesionArr.push(x);
    });
  }
  getAns() {
    this.firebase.stressAns().subscribe((data) => {
      let x = data.map((e) => {
        e.payload.ref.on('value',snap => {
          this.ansArr.push(snap.val());
          // console.log(snap.val());
        })
      });
    });
  }

  calScore(score,event){
    if (this.Qindex < this.qArrLenght) {
      this.Qindex++;
      this.total += score
      console.log('index',this.Qindex);
      console.log('score',score);
      if (this.total >= 0 && this.total <= 4) {
        this.stressResult = 'เครียดน้อย';
      } else if (this.total >= 5 && this.total <= 7) {
        this.stressResult = 'เครียดปานกลาง';
      } else if (this.total >= 8 && this.total <= 9) {
        this.stressResult = 'เครียดมาก';
      } else if (this.total >= 10 && this.total <= 15) {
        this.stressResult = 'เครียดมากที่สุด';
      }
    } else {
      event.preventDefault(); 
    }
  }
  getUserByID(){
    this.firebase.getProfile(this.userLogged.uid).subscribe(
      data=> {
         data.map(item => {
          this.profile = item.payload.val();
         })
         console.log(this.profile);
         
      }
    )
  }
  submit(){
    this.totalcard= true;
    document.getElementById('send').style.display = "none"
    let data = {
      totalScore : this.total,
      gender : this.profile.gender,
      result: this.stressResult,
      completeDT : this.currentDate,
      user: this.profile.user
    }
    console.log("ดาต้า: ",data);
    this.firebase.stressSubmit(data,this.userLogged.uid);
  }
  
  backtoMain(){
    this.router.navigate(['/tabs/tab1'])
  }
}
