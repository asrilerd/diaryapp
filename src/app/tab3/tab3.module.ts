import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab3Page } from './tab3.page';

import { Tab3PageRoutingModule } from './tab3-routing.module'
import { ReadNewsComponent } from './components/read-news/read-news.component';
// import { ReadNewsComponent } from './components/read-news/read-news.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: Tab3Page }, 
      { path:'news/:id', component: ReadNewsComponent},
    ]),
    Tab3PageRoutingModule,
    
  ],
  declarations: [Tab3Page,ReadNewsComponent]
})
export class Tab3PageModule {}
