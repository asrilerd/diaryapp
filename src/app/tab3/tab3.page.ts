import { Component, OnInit } from '@angular/core';
import { FirebaseService } from './../services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  newsList;
  constructor(
    private firebase:FirebaseService,
    private router: Router,
  ) {}

  ngOnInit(){
    this.showMyNews();
  }
  showMyNews(){
    this.firebase.getNews().subscribe( data => {
         this.newsList = data.map( e=> {
           return {
             key : e.key ,
             value : e.payload.val()
           }
         });
         console.log(this.newsList);
         
    });
  }
  
  readNews(item){
    console.log( item.value , item.key);
    this.router.navigate([`tabs/tab3/news/${item.key}`])
  }
}
