import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Subject } from 'rxjs';
import { AuthService } from './../../../auth/auth.service';


@Component({
  selector: 'app-read-news',
  templateUrl: './read-news.component.html',
  styleUrls: ['./read-news.component.scss'],
})
export class ReadNewsComponent implements OnInit , OnDestroy{

  id = this.route.snapshot.paramMap.get('id');
 
  news: any={}; // ต้องเป็น any object
  private unsubscribe$ = new Subject();
  constructor(
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private db: AngularFireDatabase,
    private router: Router,
    private auth: AuthService,

  ) { }

  ngOnInit():void {
    this.getDatabyKey(this.id);   
    console.log( );
  }

  ngOnDestroy():void{
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


  getDatabyKey(id) {
    this.db.object('News/'+id)
     .snapshotChanges()
     .subscribe((data) => {
      this.news = data.payload.val();
      } )
    console.log(this.news);
    
  }

  backNewsPage(){
    console.log("go back"); 
    this.router.navigateByUrl('/tabs/tab3')
  }
}
