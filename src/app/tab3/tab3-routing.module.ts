import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab3Page } from './tab3.page';
import { ReadNewsComponent } from './components/read-news/read-news.component';

const routes: Routes = [
  {
    path: '',
    component: Tab3Page,
  },
  {
    path:'news/:id',
    component: ReadNewsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab3PageRoutingModule {}
