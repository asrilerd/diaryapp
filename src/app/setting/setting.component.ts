import { Component, OnInit } from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { SettingPassComponent } from "../setting-pass/setting-pass.component";
import { AuthService } from "./../auth/auth.service";
import { FirebaseService } from "src/app/services/firebase.service";
import { AlertController } from '@ionic/angular';

@Component({
  selector: "app-setting",
  templateUrl: "./setting.component.html",
  styleUrls: ["./setting.component.scss"],
})
export class SettingComponent implements OnInit {
  userLogged = JSON.parse(localStorage.getItem("user"));
  tgstatus1 = false;
  tgstatus2 = false;
  userStatus;
  key;
  status: boolean = true;
  constructor(
    private dialog: MatDialog,
    public auth: AuthService,
    public db: FirebaseService,
    public alertController: AlertController
  ) {}

  ngOnInit() {
    this.getProfile();
  }

  toggle1() {
    this.tgstatus1 = !this.tgstatus1;
    console.log(this.tgstatus1);
  }
  toggle2() {
    this.tgstatus2 = !this.tgstatus2;
    console.log(this.tgstatus2);
  }
  getProfile() {
    this.db.getProfile(this.userLogged.uid).subscribe((data) => {
      data.map((item) => {
        this.key = item.key;
        console.log(this.key);
      });
    });
  }
  setPass() {
    const dialogConfig: MatDialogConfig = {
      data: this.userLogged,
    };
    this.dialog.open(SettingPassComponent, dialogConfig);
  }

  changeStatus(status) {
    console.log(status);
    if (status === "open") {
      this.userStatus = true;
    } else if (status === "close") {
      this.userStatus = false;
    }
    console.log(this.userStatus);
    this.presentAlert().then(
      );
    }
    async presentAlert() {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'ต้องการระงับการใช้งานใช่หรือไม่',
        subHeader: '',
        message: 'หากต้องการระงับการใช้งานกรุณากดตกลง',
        buttons: [
          {
            text: 'ยกเลิก',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'ตกลง',
            handler: () => {
              console.log('Confirm Okay');
              this.db.updateUser(this.key, { status: this.userStatus })
              .then( () => {
                return this.auth.SignOut()
              });
          }
        }
      ]
    });

    await alert.present();
  }
}
