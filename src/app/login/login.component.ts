import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { AuthService } from "./../auth/auth.service";
import { Router } from "@angular/router";
import * as firebase from "firebase";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  get email() {
    return this.form.get("email");
  }
  get pass() {
    return this.form.get("password");
  }
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initform();
  }

  private initform() {
    this.form = this.fb.group({
      email: null,
      password: null,
    });
  }

  login() {
    this.auth.SignIn(this.email.value, this.pass.value).then(() => {
      firebase
        .database()
        .ref("User")
        .orderByChild("email")
        .equalTo(this.email.value)
        .on("child_added", (snap) => {
          console.log("ค่าทั้งหมด", snap.val());
          if (snap.val().role == "user") {
            this.router.navigate(["tabs/tab1"]);
          }
          this.form.reset();
        });
    });
  }
  signup() {
    this.router.navigate(["/signup"]);
  }
}
