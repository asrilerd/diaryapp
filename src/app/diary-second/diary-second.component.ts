import { Component, OnInit } from '@angular/core';
import { CalendarComponentOptions, DayConfig } from 'ion2-calendar';
import { FormBuilder } from '@angular/forms';
import { FirebaseService } from '../services/firebase.service';
import { AuthService } from '../auth/auth.service';
import { Media } from '@ionic-native/media/ngx';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { File } from '@ionic-native/file/ngx';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Message } from '@progress/kendo-angular-conversational-ui';
import { async } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogConfig } from '@angular/material/dialog';
import { CalendarDialogComponent } from '../calendar-dialog/calendar-dialog.component';
@Component({
  selector: 'app-diary-second',
  templateUrl: './diary-second.component.html',
  styleUrls: ['./diary-second.component.scss'],
})
export class DiarySecondComponent implements OnInit {
  date: string;
  type: 'moment';
  eventSource: DayConfig[] = [] ;
  dateTime = new Date();
  currentDate = this.dateTime.toLocaleDateString();
  currentTime = this.dateTime.toLocaleTimeString();
  public userLogged = JSON.parse(localStorage.getItem('user'));
  src:string = "./assets/icon/034-crying.png";
  diary ;
  iconName;
  iconScore;
  calender;
  options : CalendarComponentOptions ;

  constructor(
    private fb:FormBuilder , 
    private firebase:FirebaseService , 
    private auth:AuthService , 
    private media:Media , 
    private file : File,
    private db: AngularFireDatabase,
    public alertController: AlertController ,
    private router :Router,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.getDiaryforCalendar();
  }
  dateDiary(){
     this.options= {
      from: this.dateTime.setDate(this.dateTime.getDate() - 60),
      pickMode: 'single',
      daysConfig: this.eventSource
    };
  }

  backtoMain(){
    this.router.navigate(['tabs/tab1'])
  }
  findMe(lat,lng){
    console.log(lat,lng);
    this.router.navigate([`maping/${lat}/${lng}`])
  }

  selectDate(Date){
    console.log('date',Date._d.toLocaleDateString() );
    const dateTimeDiary = Date._d.toLocaleDateString()
    this.getDiary(dateTimeDiary,this.userLogged.uid);
    console.log(Date);
    
  }
  
  createDiary(){
    this.router.navigate(['diaryFirst'])
  }
  getDiaryforCalendar(){
    this.firebase.getDiaryForCalendar(this.userLogged?.uid).subscribe((data) => {
      data.forEach( item => {
          // Object.keys(item).map( key => item[key]).map( order=> {
            this.calender = item 
            console.log('date of calendar' , this.calender.timeStamp);
            this.eventSource.push({ date: this.calender.timeStamp, subTitle: 'diary' }) , console.log('configDate',this.eventSource);
            this.dateDiary();
          });
        });
  }

  getDiary(dateTime?,uid?){
    this.firebase.getDiary(dateTime,uid).subscribe((data) => {
      this.diary = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      console.log(this.diary);
      console.log('Current uid :',uid);
    });
  }
 
  getnote(note,src){
    console.log(note.key);
    const dialogConfig : MatDialogConfig = {
      data: note
    }
    this.dialog.open(CalendarDialogComponent, dialogConfig);

  }


  deleteitem(pushkey){
     console.log(pushkey);
      this.firebase.deleteDiaryItem(pushkey,this.userLogged.uid).then(
        (result)=>{
            console.log(result);
        } 
      );
      // this.getDiaryforCalendar();
  }
}
