import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { AuthService } from "./../auth/auth.service";
import { HttpClient } from "@angular/common/http";
import { FirebaseService } from "src/app/services/firebase.service";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { TestwarningDialogComponent } from "../testwarning-dialog/testwarning-dialog.component";
declare var kommunicate: any;
@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.scss"],
})
export class ChatComponent implements OnInit {
  userLogged = JSON.parse(localStorage.getItem("user"));
  mess;
  get userInput() {
    return this.form.get("userInput");
  }
  form: FormGroup;
  lastestDepress;
  lastestStress;
  lastestSuicide;
  lastestHappy;
  date = new Date();
  currentDate = this.date.valueOf();
  url = "https://us-central1-moodtracker-20fb0.cloudfunctions.net/helloworld";
  messages: any[] = [];
  sendUidUrl =
    "https://us-central1-moodtracker-20fb0.cloudfunctions.net/findUid";
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    public http: HttpClient,
    public firebase: FirebaseService,
    public route: Router,
    public dialog: MatDialog
  ) {}

  private initform() {
    this.form = this.fb.group({
      userInput: null,
    });
  }

  ngOnInit() {
    this.getDepression();
    this.getStress();
    this.getSuicide();
    this.getHappy();
    // this.http.post(this.sendUidUrl,this.userLogged.uid);
    // (function(d, m){
    //   var kommunicateSettings = {"appId":"18d3d3d80d9d5a1eba23f84d136412da7","popupWidget":true,"automaticChatOpenOnNavigation":true};
    //   var s = document.createElement("script"); s.type = "text/javascript"; s.async = true;
    //   s.src = "https://widget.kommunicate.io/v2/kommunicate.app";
    //   var h = document.getElementsByTagName("head")[0]; h.appendChild(s);
    //   window.kommunicate = m; m._globals = kommunicateSettings;
    // })
    // (document, window.kommunicate || {});
    // this.userLogged
  }

  test9Q() {
    const differentDate = Math.round(
      Math.abs((this.lastestDepress - this.currentDate) / (24 * 60 * 60 * 1000))
    );
    console.log("differentDate : ", differentDate);
    if (differentDate < 14) {
      console.log("you cannot go");
      this.dialog.open(TestwarningDialogComponent);
    } else {
      this.route.navigate(["/depression"]);
    }
  }
  test8Q() {
    const differentDate = Math.round(
      Math.abs((this.lastestSuicide - this.currentDate) / (24 * 60 * 60 * 1000))
    );
    console.log("differentDate : ", differentDate);
    if (differentDate < 14) {
      console.log("you cannot go");
      this.dialog.open(TestwarningDialogComponent);
    } else {
      this.route.navigate(["/suicide"]);
    }
  }
  testST5() {
    const differentDate = Math.round(
      Math.abs((this.lastestStress - this.currentDate) / (24 * 60 * 60 * 1000))
    );
    console.log("differentDate : ", differentDate);
    if (differentDate < 14) {
      console.log("you cannot go");
      this.dialog.open(TestwarningDialogComponent);
    } else {
      this.route.navigate(["/stress"]);
    }
  }
  testTMHI() {
    const differentDate = Math.round(
      Math.abs((this.lastestHappy - this.currentDate) / (24 * 60 * 60 * 1000))
    );
    console.log("differentDate : ", differentDate);
    if (differentDate < 14) {
      console.log("you cannot go");
      this.dialog.open(TestwarningDialogComponent);
    } else {
      this.route.navigate(["/happy"]);
    }
  }

  getDepression() {
    this.firebase.depressionHistory(this.userLogged.uid).subscribe((data) => {
      data.map((e) => {
        e.payload.ref
          .orderByChild("completeDT")
          .limitToLast(1) //query จากวันที่อัพสุดท้าย limit 1 once แสดง1อัน
          .once("child_added", (snap) => {
            this.lastestDepress = snap.val().completeDT;
            console.log("formDepress", snap.val().completeDT);
          });
      });
    });
  }
  getStress() {
    this.firebase.stressHistory(this.userLogged.uid).subscribe((data) => {
      data.map((e) => {
        e.payload.ref
          .orderByChild("completeDT")
          .limitToLast(1) //query จากวันที่อัพสุดท้าย limit 1 once แสดง1อัน
          .once("child_added", (snap) => {
            this.lastestStress = snap.val().completeDT;
            console.log("formStress", snap.val().completeDT);
          });
      });
    });
  }
  getHappy() {
    this.firebase.happyHistory(this.userLogged.uid).subscribe((data) => {
      data.map((e) => {
        e.payload.ref
          .orderByChild("completeDT")
          .limitToLast(1) //query จากวันที่อัพสุดท้าย limit 1 once แสดง1อัน
          .once("child_added", (snap) => {
            this.lastestHappy = snap.val().completeDT;
            console.log("formHappy", snap.val().completeDT);
          });
      });
    });
  }
  getSuicide() {
    this.firebase.suicideHistory(this.userLogged.uid).subscribe((data) => {
      data.map((e) => {
        e.payload.ref
          .orderByChild("completeDT")
          .limitToLast(1) //query จากวันที่อัพสุดท้าย limit 1 once แสดง1อัน
          .once("child_added", (snap) => {
            this.lastestSuicide = snap.val().completeDT;
            console.log("formSuicide", snap.val().completeDT);
          });
      });
    });
  }
}
