import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-testhistory',
  templateUrl: './testhistory.component.html',
  styleUrls: ['./testhistory.component.scss'],
})
export class TesthistoryComponent implements OnInit {
  public userLogged = JSON.parse(localStorage.getItem('user'));
  uid = this.userLogged.uid
  depress=[]; stress=[]; suicide=[]; happiness=[];
  depressT=[]; stressT=[]; suicideT=[]; happinessT=[];
  constructor( private firebase:FirebaseService) { }

  ngOnInit() {
      this.getStressHistory();
      this.getdepressHistory();
      this.getsuicideHistory();
      this.gethappyHistory();
  }
  getStressHistory(){
    this.firebase.stressHistory(this.uid).subscribe((data) => {
       data.map((e) => {
        e.payload.ref.on('child_added',snap=>{
          this.stress.push(snap.val());
          const day = new Date(snap.val().completeDT).toLocaleDateString();  
          this.stressT.push(day);
          console.log(day);
        })
      });
    });
  }
  getdepressHistory(){
    this.firebase.depressionHistory(this.uid).subscribe((data) => {
      data.map((e) => {
       e.payload.ref.on('child_added',snap=>{
         this.depress.push(snap.val());
         const day = new Date(snap.val().completeDT).toLocaleDateString();  
          this.depressT.push(day);
          console.log(day);
       })
     });
   });
  }
  getsuicideHistory(){
    this.firebase.suicideHistory(this.uid).subscribe((data) => {
      data.map((e) => {
       e.payload.ref.on('child_added',snap=>{
         this.suicide.push(snap.val());
         const day = new Date(snap.val().completeDT).toLocaleDateString();  
          this.suicideT.push(day);
          console.log(day);
       })
     });
   });
  }
  gethappyHistory(){
    this.firebase.happyHistory(this.uid).subscribe((data) => {
      data.map((e) => {
       e.payload.ref.on('child_added',snap=>{
         this.happiness.push(snap.val());
         const day = new Date(snap.val().completeDT).toLocaleDateString();  
          this.happinessT.push(day);
          console.log(day);
       })
     });
   });
  }
}
