import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-happy',
  templateUrl: './happy.component.html',
  styleUrls: ['./happy.component.scss'],
})
export class HappyComponent implements OnInit {

  quesion;
  quesionArr = [];
  ansArr = [];
  arrMinor = [];
  Qindex: number = 0;
  qArrLenght: number;
  date = new Date();
  currentDate = this.date.valueOf();
  total = 0;
  index = 1;
  happyResult: string;
  profile;
  totalcard = false;
  constructor(private firebase: FirebaseService , private router: Router) {}

  public userLogged = JSON.parse(localStorage.getItem("user"));
  ngOnInit() {
    this.getQuesion();
    this.getUserByID();
    this.getAns();
    setTimeout(() => {
      this.quesionArr.map((r) => {
        this.qArrLenght = r.length;
        // console.log(this.qArrLenght);
      });
    }, 2000);
  }
  getQuesion() {
    this.firebase.happyQ().subscribe((data) => {
      let x = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      this.quesionArr.push(x);
      // console.log(this.quesionArr);
    });
  }
  getAns() {
    this.firebase.happyAns().subscribe((data) => {
      let x = data.map((e) => {
        e.payload.ref.child("answer").on("value", (snap) => {
          this.ansArr.push(snap.val());
          // console.log("array คำตอบ",this.ansArr);
          this.anstoArr(0);
        });
      });
    });
  }
  anstoArr(index) {
    this.arrMinor.shift();
    this.arrMinor.push(this.ansArr[index]);
    console.log(this.arrMinor);
  }

  calScore(score, event) {
    if (this.Qindex < this.qArrLenght) {
      this.Qindex++;
      this.total += score;
      // console.log('index',this.Qindex);
      // console.log("score", score);
      this.anstoArr(this.index);
      this.index++;
      if (this.total >= 51 && this.total <= 60) {
        this.happyResult = 'สุขภาพจิตดีกว่าคนทั่วไป';
      } else if (this.total >= 44 && this.total <= 50) {
        this.happyResult = 'สุขภาพจิตเท่ากับคนทั่วไป';
      } else if (this.total <= 43) {
        this.happyResult = 'สุขภาพจิตต่ำกว่าคนทั่วไป';
      }
    } else {
      event.preventDefault();
    }
  }
  getUserByID(){
    this.firebase.getProfile(this.userLogged.uid).subscribe(
      data=> {
         data.map(item => {
          this.profile = item.payload.val();
         })
         console.log(this.profile);
         
      }
    )
  }
  submit() {
    this.totalcard= true;
    console.log("คะแนน: ", this.total);
    document.getElementById('send').style.display = "none"
    let data = {
      totalScore: this.total,
      gender: this.profile.gender,
      result: this.happyResult,
      completeDT: this.currentDate,
      user: this.profile.user,
    };
    console.log(data);
    this.firebase.happySubmit(data,this.userLogged.uid);
  }

  backtoMain(){
    this.router.navigate(['/tabs/tab1'])
  }
}
