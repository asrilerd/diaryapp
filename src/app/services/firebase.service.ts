import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class FirebaseService {
  constructor(private db: AngularFireDatabase, public http: HttpClient) {
   
  }

  getProfile(uid?){
    return this.db.list("User" , ref => ref.orderByChild('uid').equalTo(uid)).snapshotChanges();
  }

  getNews() {
    return this.db.list("News").snapshotChanges();
  }
  getNewsAd() {
    return this.db.list("News", ref => ref.limitToLast(4)).snapshotChanges();
  }

  getMap() {
    return this.db.list("Location").snapshotChanges();
  }
  getActivity() {
    return this.db.list("Activity").snapshotChanges();
  }
  getDoc() {
    return this.db.list("Document").snapshotChanges();
  }
  getDiary(dateTime, uid) {
    return this.db
      .list(`Diary/${uid}`, (ref) => ref.orderByChild("date").equalTo(dateTime))
      .snapshotChanges();
  }

  ///////////////////////
  getIconName(uid, startTime?, endTime?) {
    return this.db
      .list(`Diary/${uid}`, (ref) =>
        ref.orderByChild("timeStamp").startAt(startTime).endAt(endTime)
      )
      .valueChanges();
  }

  getDiaryForStat(uid, startTime?, endTime?) {
    return this.db
      .list(`Diary/${uid}`, (ref) =>
        ref.orderByChild("timeStamp").startAt(startTime).endAt(endTime)
      )
      .valueChanges();
  }
  getscore(uid) {
    return this.db.list(`Diary/${uid}`).valueChanges();
  }

  /////////////////////////

  getDiaryForCalendar(uid) {
    return this.db.list(`Diary/${uid}`).valueChanges();
  }

  addUser(data) {
    return this.db.list("User").push(data.value);
  }
  addDiary(data, uid) {
    return this.db.list(`Diary/${uid}`).push(data.value);
  }

  updateUser(key:string , data:any) {
    return this.db.list('User').update(key,data);
   }
   
  deleteDiaryItem(key: string, uid) {
    return this.db.list(`Diary/${uid}`).remove(key);
  }
  /////quesion////
  stressQ(){
    return this.db.list('stressQuesion').snapshotChanges();
  }
  stressAns(){
    return this.db.list('stressQuesion/answer').snapshotChanges();
  }
  depressQ(){
    return this.db.list('depresQuesion').snapshotChanges();
  }
  depressAns(){
    return this.db.list('depresQuesion/answer').snapshotChanges();
  }
  happyQ(){
    return this.db.list('happyQuesion').snapshotChanges();
  }
  happyAns(){
    return this.db.list('happyQuesion').snapshotChanges();
  }
  suicideQ(){
    return this.db.list('suicideQuesion').snapshotChanges();
  }
  suicideAns(){
    return this.db.list('suicideQuesion').snapshotChanges();
  }
  // happyAns(){
  //   return this.db.list('happyQuesion/answer').snapshotChanges();
  // }
  //answer//
  stressSubmit(data,uid){
    return this.db.list(`stressScore/${uid}/result`).push(data);
  }
  depressSubmit(data,uid){
    return this.db.list(`depressScore/${uid}/result`).push(data);
  }
  happySubmit(data,uid){
    return this.db.list(`happinessScore/${uid}/result`).push(data);
  }
  suicideSubmit(data,uid){
    return this.db.list(`suicideScore/${uid}/result`).push(data);
  }

  //history//
  stressHistory(uid){
    return this.db.list(`stressScore/${uid}`).snapshotChanges();
  }
  depressionHistory(uid){
    return this.db.list(`depressScore/${uid}`).snapshotChanges();
  }
  suicideHistory(uid){
    return this.db.list(`suicideScore/${uid}`).snapshotChanges();
  }
  happyHistory(uid){
    return this.db.list(`happinessScore/${uid}`).snapshotChanges();
  }
}
