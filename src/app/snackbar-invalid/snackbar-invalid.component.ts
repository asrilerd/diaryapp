import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-snackbar-invalid',
  templateUrl: './snackbar-invalid.component.html',
  styleUrls: ['./snackbar-invalid.component.scss'],
})
export class SnackbarInvalidComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
