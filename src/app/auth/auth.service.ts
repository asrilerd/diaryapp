import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { ActivatedRoute } from '@angular/router';
import { auth } from 'firebase';
import { FormGroup } from '@angular/forms';
import * as firebase from 'firebase';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public userData: any; // Save logged in user data
  id;
  user: any;
  userList: any;
  hotel: any;
  uid: any;
  userLogged;
  static userData: any;
  constructor(
    public afAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone, // NgZone service to remove outside scope warning
    private db: AngularFireDatabase,
    private ActivatedRoute: ActivatedRoute
  ) {
    this.afAuth.authState.subscribe((user) => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));       
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    });
   }
   SignIn(email, password) {
    return this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then((result) => {
        console.log('login', result);
        this.userLogged = result;
        localStorage.setItem('user', JSON.stringify(result));
      })
      .catch((error) => {
        window.alert(error.message);
      });
  }
  // this.ngZone.run(() => {
  //   this.router.navigate(['chatbot']);
  //   console.log(result);
  //   this.userLogged = result ;
  //   localStorage.setItem('user', JSON.stringify(result));

  // Sign up with email/password
  SignUp(email, password, data:FormGroup) {
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        this.db.list('/User').push(data);
      })
      .catch((error) => {
        window.alert(error.message);
      });
  }

   // Sign out
   SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    });
  }
   // Returns true when user is looged in and email is verified
   get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null ? true : false;
  }

   // Auth logic to run auth providers
   AuthLogin(provider) {
    return this.afAuth
      .signInWithPopup(provider)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['home']);
        });
        //  this.SetUserData(result.user);
      })
      .catch((error) => {
        window.alert(error);
      });
  }


  // Sign in with Google
  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }

  
  SetUserData(user) {
    const userRef: AngularFireList<any> = this.db.list(`Admin/${user.uid}`);
    const userData: any = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
    };
    return userRef.set(userData, {
      merge: true,
    });
  }

}
