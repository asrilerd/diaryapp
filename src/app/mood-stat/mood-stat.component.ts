import { Component, OnInit } from "@angular/core";
import { Chart } from "chart.js";
import { title } from "process";
import { GoogleChartInterface } from "ng2-google-charts";
import { FirebaseService } from "../services/firebase.service";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { AppSettings } from "../app-settings";
import { Subject } from "rxjs";
import "chart.piecelabel.js";
import ChartDataLabels from "chartjs-plugin-datalabels";
import {
  map,
  takeUntil,
  filter,
  switchMap,
  ignoreElements,
} from "rxjs/operators";
import { LoginComponent } from "../login/login.component";
import { CalendarDialogComponent } from "../calendar-dialog/calendar-dialog.component";
import { NavController, ModalController } from "@ionic/angular";
import { constants } from "buffer";
import { async } from "@angular/core/testing";
import * as firebase from "firebase";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
@Component({
  selector: "app-mood-stat",
  templateUrl: "./mood-stat.component.html",
  styleUrls: ["./mood-stat.component.scss"],
})
export class MoodStatComponent implements OnInit {
  private unsubscribe$ = new Subject();
  form: FormGroup;
  lineChart: any[]; // เก็บค่า
  angry: number[] = [];
  sad: number[] = [];
  lonely: number[] = [];
  bad: number[] = [];
  bored: number[] = [];
  tired: number[] = [];
  chill: number[] = [];
  happy: number[] = [];
  userLogged = JSON.parse(localStorage.getItem("user"));
  uidUser = this.userLogged.uid;
  barStatus = false;
  barValue = "horizontalBar";
  googlebar = "BarChart";
  barline = false;
  icon;
  score: number = 0;
  title;
  type;
  data;
  columnNames;
  options;
  drawOnY = true;
  drawTicksY = true;
  drawOnX = false;
  startTimestamp = null;
  endTimestamp = null;
  currentDate = new Date();
  startTimestr;
  endTimestr;
  dateStart;
  dateEnd;
  searchValue;
  cardStatus = true;
  iconName: string[] = [];
  searchdateArr = [];
  iconwithScore;
  img;
  get startTiming() {
    return this.form.get("start");
  }
  get endTiming() {
    return this.form.get("end");
  }
  iconShow = [
    "./assets/icon/051-angry.png",
    "./assets/icon/051-cry.png",
    "./assets/icon/051-sad.png",
    "./assets/icon/051-sad-1.png",
    "./assets/icon/051-unamused.png",
    "./assets/icon/051-tired.png",
    "./assets/icon/051-smile-1.png",
    "./assets/icon/051-smiling.png",
  ];
  x = 0;
  constructor(
    private firebase: FirebaseService,
    private dialog: MatDialog,
    public navCtrl: NavController,
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.initform();
    this.startTimestamp = this.currentDate.valueOf();
    this.endTimestamp = this.currentDate.valueOf();
    this.getIcon();
    this.getScoreIcon();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  back() {
    this.router.navigate(["/tabs/tab1"]);
  }

  initform() {
    this.form = this.fb.group({
      start: null,
      end: null,
    });
  }

  checkBtn() {
    console.log("hey");
    const x = this.startTiming.value.valueOf();
    const y = this.endTiming.value.toLocaleDateString();
    console.log(x, y);
  }

  iconScore() {
    if (this.iconwithScore == 0) {
      this.x = 1;
    } else if (this.iconwithScore >= 1 && this.iconwithScore <= 3) {
      this.x = 1;
    } else if (this.iconwithScore >= 4 && this.iconwithScore <= 6) {
      this.x = 2;
    } else if (this.iconwithScore >= 7 && this.iconwithScore <= 8) {
      this.x = 4;
    } else if (this.iconwithScore >= 9) {
      this.x = 7;
    }
  }

  dialogDate() {
    // modal.onDidDismiss().then((result) => {
    //   if (result.data && result.data.event) {
    //     // let event = result.data.event;
    //     // let from = event.from;
    //     // let to = event.to;

    //     // this.dateStart = from._d.getDate();
    //     // this.dateEnd = to._d.getDate();

    //     // this.startTimestamp = from._i;
    //     // this.endTimestamp = to._i;

    //     // this.startTimestr = from._d.toLocaleDateString();
    //     // this.endTimestr = to._d.toLocaleDateString();

    //     // console.log( 'timestamp :', this.startTimestamp, this.endTimestamp);
    //     // console.log ( 'datwstr : ',this.startTimestr, this.endTimestr);
    //     // console.log ( 'datetime : ',from._d , to._d );
    //     // this.searchValue = this.startTimestr + " - " + this.endTimestr;

    //   }
    this.startTimestamp = this.startTiming.value.valueOf();
    this.endTimestamp = this.endTiming.value.valueOf();
    this.iconName = [];
    this.angry = [];
    this.bad = [];
    this.lonely = [];
    this.sad = [];
    this.bored = [];
    this.tired = [];
    this.chill = [];
    this.happy = [];
    this.searchdateArr = [];
    this.score = 0;
    this.getIcon();
    this.getScoreIcon();
  }

  getIcon() {
    this.firebase
      .getIconName(this.userLogged.uid, this.startTimestamp, this.endTimestamp)
      .subscribe((data) => {
        return (
          data.map((item: item) => {
            // this.iconwithScore = item.icon.iconName;
            const iconName = item.icon.iconName;
            this.iconName.push(iconName);
            if (iconName === "angry") {
              this.angry.push(iconName);
            } else if (iconName === "sad") {
              this.sad.push(iconName);
            } else if (iconName === "lonely") {
              this.lonely.push(iconName);
            } else if (iconName === "blue") {
              this.bad.push(iconName);
            } else if (iconName === "bored") {
              this.bored.push(iconName);
            } else if (iconName === "tired") {
              this.tired.push(iconName);
            } else if (iconName === "chill") {
              this.chill.push(iconName);
            } else if (iconName === "happy") {
              this.happy.push(iconName);
            }
            // this.circleChartMood()
            // this.lineChartMood(),
            // this.columnChartMood(),
            // this.googleChart();
          }),
          console.log(
            " data in array",
            this.angry,
            this.bad,
            this.bored,
            this.chill,
            this.sad,
            this.happy,
            this.lonely,
            this.tired
          ),
          this.check(this.iconName)
        );
      });
  }

  async check(iconname) {
    if (iconname.length === 0) {
      this.cardStatus = false;
      console.log("no data", this.cardStatus);
    } else {
      console.log("icon na ka : ", iconname);
      return (
        (this.cardStatus = true),
        console.log(this.cardStatus),
        setTimeout(() => {
          this.circleChartMood(),
            this.lineChartMood(),
            this.columnChartMood(),
            this.googleChart();
        }, 20)
      );
    }
  }

  googleChart() {
    this.title = "";
    this.type = this.googlebar;
    this.data = [
      ["😄 ", this.angry.length],
      ["🙄 ", this.sad.length],
      ["😄 ", this.lonely.length],
      ["😄 ", this.bad.length],
      ["😄 ", this.bored.length],
      ["😄 ", this.tired.length],
      ["😄 ", this.chill.length],
      ["😄 ", this.happy.length],
    ];
    this.columnNames = ["", ""];
    this.options = {
      bar: { groupWidth: "70%" },
      legend: { position: "none" },
      // height: 200,
      colors: ["#50c0c0"],
      // is3D: true
    };
  }

  toggleBarChart() {
    this.barStatus = !this.barStatus;
    this.cardStatus = !this.cardStatus;
    if (this.barStatus === false) {
      this.barValue = "horizontalBar";
      this.googlebar = "BarChart";
      // this.drawOnY = false;
      // this.drawTicksY = false;
      // this.drawOnX = true;
      console.log(this.googlebar);
    } else if (this.barStatus === true) {
      this.barValue = "bar";
      this.googlebar = "ColumnChart";
      // this.drawOnY = true;
      // this.drawTicksY = true;
      // this.drawOnX = false;
      console.log(this.googlebar);
    }

    // this.getIcon();
    this.lineChartMood();
    this.googleChart();
    this.circleChartMood();
  }

  getScoreIcon() {
    this.firebase
      .getDiaryForStat(
        this.userLogged.uid,
        this.startTimestamp,
        this.endTimestamp
      )
      .subscribe((data) => {
        console.log("subscribeData :", data);
        this.score = 0;
        if (data.length === 0) {
          // this.score = 0
        } else {
          data.forEach((item: item) => {
            this.score = this.score + item.icon.iconScore;
          });
          this.score = this.score / data.length;
          setTimeout(() => {
            this.iconwithScore = this.score;
            this.iconScore();
          }, 500);
        }
      });
  }

  lineChartMood() {
    this.lineChart = new Chart("lineChart", {
      type: "horizontalBar",
      data: {
        // yLabels: ["Python", "Octave/MATLAB", "JavaScript"],
        // xLabels: ["Beginner", "Intermediate", "Advanced"],
        labels: [
          "angry",
          "sad  ",
          "lonely ",
          "feeling blue  ",
          "bored  ",
          "tired  ",
          "chill  ",
          "happy  ",
        ], //แกน x
        datasets: [
          {
            label: "",
            data: [
              this.angry.length,
              this.sad.length,
              this.lonely.length,
              this.bad.length,
              this.bored.length,
              this.tired.length,
              this.chill.length,
              this.happy.length,
            ],
            fill: true,
            lineTension: 0,
            borderWidth: 1,
            borderColor: "rgba(75, 192, 192, 1)",
            backgroundColor: "#50c0c0",
          },
        ],
      },
      plugins: [ChartDataLabels],
      options: {
        maintainAspectRatio: false,
        tooltips: {
          enabled: false,
        },
        legend: {
          display: false,
        },
        animation: {
          easing: "easeOutQuint",
        },
        scales: {
          yAxes: [
            {
              ticks: {
                precision: 0,
                fontFamily: "FontAwesome",
                fontStyle: "normal",
                fontColor: "red",
                fill: false,
                fontSize: 40,
                callback: function (value, index, values) {
                  if (index === 0) {
                    return "\uf5b4";
                  } else if (index === 1) {
                    return "\uf5b4";
                  } else if (index === 2) {
                    return "\uf5a4";
                  } else if (index === 3) {
                    return "\uf57a";
                  } else if (index === 4) {
                    return "\uf5a5";
                  } else if (index === 5) {
                    return "\uf5c8";
                  } else if (index === 6) {
                    return "\uf598";
                  } else if (index === 7) {
                    return "\uf58c";
                  }
                },
              },
              gridLines: {
                drawOnChartArea: false,
                drawTicks: this.drawOnY,
              },
            },
          ],
          xAxes: [
            {
              ticks: {
                beginAtZero: true,
                display: true,
                max: 4,
                // fontFamily: "FontAwesome",
                fontSize: 20,
                callback: function (value, index, values) {
                  return value;
                },
              },
              gridLines: {
                drawOnChartArea: true,
                drawTicks: false,
              },
            },
          ],
        },
        plugins: {
          datalabels: {
            color: "#111",
            textAlign: "center",
            formatter: function (value, index, values) {
              if (value == !0) {
                return "";
              } else {
                return "";
              }
            },
          },
        },
      },
    });
  }

  ///line///

  columnChartMood() {
    this.lineChart = new Chart("BarChart", {
      type: "line",
      data: {
        labels: [
          "angry ",
          "sad ",
          "lonely ",
          "feeling blue ",
          "bored ",
          "tired ",
          "chill ",
          "happy ",
        ], //แกน x
        datasets: [
          {
            label: "",
            data: [
              this.angry.length,
              this.sad.length,
              this.lonely.length,
              this.bad.length,
              this.bored.length,
              this.tired.length,
              this.chill.length,
              this.happy.length,
            ],

            fill: true,
            lineTension: 0.2,
            borderWidth: 1,
            borderColor: "rgba(75, 192, 192, 1)",
            backgroundColor: "rgba(75, 192, 192, 0.2)",
            steppedLine: false,
          },
        ],
      },
      plugins: [ChartDataLabels],
      options: {
        maintainAspectRatio: false,
        tooltips: {
          enabled: false,
        },
        legend: {
          display: false,
        },
        animation: {
          easing: "easeOutQuint",
        },
        scales: {
          yAxes: [
            {
              ticks: {
                precision: 0,
                beginAtZero: true,
              },
              gridLines: {
                drawOnChartArea: true,
                drawTicks: true,
              },
            },
          ],
          xAxes: [
            {
              ticks: {
                beginAtZero: true,
                display: true,
                max: 4,
                fontFamily: "FontAwesome",
                fontSize: 20,
                callback: function (value, index, values) {
                  if (index === 0) {
                    return "\uf556";
                  } else if (index === 1) {
                    return "\uf5b4";
                  } else if (index === 2) {
                    return "\uf5a4";
                  } else if (index === 3) {
                    return "\uf57a";
                  } else if (index === 4) {
                    return "\uf5a5";
                  } else if (index === 5) {
                    return "\uf5c8";
                  } else if (index === 6) {
                    return "\uf598";
                  } else if (index === 7) {
                    return "\uf58c";
                  }
                },
              },
              gridLines: {
                drawOnChartArea: false,
                drawTicks: true,
              },
            },
          ],
        },
        plugins: {
          datalabels: {
            color: "#111",
            textAlign: "center",
            formatter: function (value, index, values) {
              if (value == !0) {
                return "";
              } else {
                return "";
              }
            },
          },
        },
      },
    });
  }

  circleChartMood() {
    const x =
      this.angry.length +
      this.sad.length +
      this.lonely.length +
      this.bad.length +
      this.bored.length +
      this.tired.length +
      this.chill.length +
      this.happy.length;
    this.lineChart = new Chart("circleChart", {
      type: "doughnut",
      data: {
        labels: [
          "angry",
          "sad",
          "lonely",
          "feeling blue",
          "bored",
          "tired",
          "chill",
          "happy",
        ], //แกน x
        datasets: [
          {
            label: "",
            data: [
              this.angry.length,
              this.sad.length,
              this.lonely.length,
              this.bad.length,
              this.bored.length,
              this.tired.length,
              this.chill.length,
              this.happy.length,
            ],
            fill: true,
            lineTension: 0.2,
            // borderColor: "rgba(75, 192, 192, 1)",
            backgroundColor: [
              "rgb(254,99,73)",
              "gold",
              "rgb(57,122,241)",
              "pink",
              "rgba(75, 192, 192)",
              "rgb(66,181,164)",
              "lightblue",
              "rgb(95,103,236)",
            ],
            borderWidth: 1,
          },
        ],
      },
      plugins: [ChartDataLabels],
      options: {
        rotation: 1 * Math.PI,
        circumference: 1 * Math.PI,
        legend: {
          display: false,
        },
        tooltip: {
          enabled: false,
        },
        cutoutPercentage: 60,
        plugins: {
          datalabels: {
            color: "white",
            textAlign: "center",
            font: {
              lineHeight: 1.6,
            },
            formatter: function (value, index, values) {
              if (value == !null) {
                let i = (value * 100) / x;
                // return i.toFixed(2) + "%";
                return "";
              } else {
                return "";
              }
            },
          },
        },
      },
      scale: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    });
  }
}
