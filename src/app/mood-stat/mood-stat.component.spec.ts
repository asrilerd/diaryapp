import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MoodStatComponent } from './mood-stat.component';

describe('MoodStatComponent', () => {
  let component: MoodStatComponent;
  let fixture: ComponentFixture<MoodStatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoodStatComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MoodStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
