// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false ,
  firebase: {
    apiKey: "AIzaSyCqBau3CoAgOrUUux7iwl8Doz6VhcPVk04",
    authDomain: "moodtracker-20fb0.firebaseapp.com",
    databaseURL: "https://moodtracker-20fb0.firebaseio.com",
    projectId: "moodtracker-20fb0",
    storageBucket: "moodtracker-20fb0.appspot.com",
    messagingSenderId: "803370525269",
    appId: "1:803370525269:web:0ab762860729a2e16e2adc",
  },
  dialogflow: {
    angularBot: '3f70289bec54341563055d206f40a946612ffeb7'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
